package com.holdfast.medication.activity.main.view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.holdfast.medication.MedicationApplication;
import com.holdfast.medication.R;
import com.holdfast.medication.activity.main.viewModel.MainViewModel;
import com.holdfast.medication.activity.medicamentCourseInformation.view.MedicamentCourseActivity;
import com.holdfast.medication.model.dataClass.MedicamentCourse;
import com.holdfast.medication.notification.NotificationAlarmManager;
import com.holdfast.medication.otherClass.MedicamentCoursePOJO;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class MedicamentCourseAdapter extends RecyclerView.Adapter<MedicamentCourseAdapter.MedicamentCourseViewHolder> {

    private List<MedicamentCoursePOJO> data;
    private Context ctx;
    private MainViewModel viewModel;

    public MedicamentCourseAdapter(Context ctx, MainViewModel viewModel) {
        this.data = new ArrayList<>();
        this.ctx = ctx;
        this.viewModel=viewModel;
    }


    public List<MedicamentCoursePOJO> getData() {
        return data;
    }

    public MedicamentCoursePOJO getItem(int position) {
        return data.get(position);
    }


    public void addItem(MedicamentCoursePOJO item) {
        data.add(item);
        notifyDataSetChanged();
    }

    public void setItems(List<MedicamentCoursePOJO> items) {
        data = items;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return data.size();
    }


    @Override
    public MedicamentCourseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_item_medicament_course, parent, false);
        return new MedicamentCourseViewHolder(view, viewModel);
    }

    @Override
    public void onBindViewHolder(MedicamentCourseViewHolder holder, int position) {
        holder.bind(data.get(position), ctx);
    }


    public static class MedicamentCourseViewHolder extends RecyclerView.ViewHolder {

        @Inject
        NotificationAlarmManager notificationAlarmManager;

        private TextView nameTextView;
        private TextView noteTextView;
        private TextView dayTakeTextView;
        private Switch statusSwitch;
        private View item;
        private MainViewModel viewModel;


        public MedicamentCourseViewHolder(View itemView, MainViewModel viewModel) {
            super(itemView);
            MedicationApplication.getComponent().inject(this);
            nameTextView = (TextView) itemView.findViewById(R.id.name_medicament_course_tv);
            noteTextView = (TextView) itemView.findViewById(R.id.note_medicament_course_tv);
            dayTakeTextView = itemView.findViewById(R.id.day_take_tv);
            statusSwitch = itemView.findViewById(R.id.switch_medicament_course_status);
            item = itemView;
            this.viewModel=viewModel;

        }

        public void bind(MedicamentCoursePOJO medicamentCoursePOJO, Context ctx) {
            nameTextView.setText(medicamentCoursePOJO.medicamentCourse.name);
            noteTextView.setText(medicamentCoursePOJO.medicamentCourse.note);
            statusSwitch.setChecked(medicamentCoursePOJO.medicamentCourse.status);
            String days =String.format(" %d", viewModel.getRemainingDaysReception(medicamentCoursePOJO));
            dayTakeTextView.setText(days);

            statusSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (isChecked) {
                            notificationAlarmManager.activateRemind(medicamentCoursePOJO.medicamentCourse.id);
                        } else {
                            notificationAlarmManager.deactivateRemind(medicamentCoursePOJO.medicamentCourse.id);
                        }
                    }
                }).start();
            });

            item.setOnClickListener(v -> {
                ctx.startActivity(new Intent(ctx, MedicamentCourseActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra(MedicamentCourse.class.getCanonicalName(),
                                medicamentCoursePOJO.medicamentCourse.id)
                );
            });

        }


    }

}
