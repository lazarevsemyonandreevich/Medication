package com.holdfast.medication.activity.main.view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.holdfast.medication.R;
import com.holdfast.medication.activity.editUnit.view.EditUnitActivity;
import com.holdfast.medication.model.dataClass.Unit;

import java.util.ArrayList;
import java.util.List;

public class UnitAdapter extends RecyclerView.Adapter<UnitAdapter.UnitViewHolder>{

    private List<Unit> data;
    private Context ctx;

    public UnitAdapter(Context ctx) {
        this.data = new ArrayList<>();
        this.ctx = ctx;
    }

    public UnitAdapter(Context ctx, List<Unit> data) {
        this.data = data;
        this.ctx = ctx;
    }

    public interface OnItemClick {
        void onClick(int position);
    }

    private OnItemClick callback;

    public void setOnItemClickCallback(OnItemClick callback) {
        this.callback = callback;
    }


    public List<Unit> getData() {
        return data;
    }

    public Unit getItem(int position) {
        return data.get(position);
    }


    public void addItem(Unit item) {
        data.add(item);
        notifyDataSetChanged();
    }

    public void setItems(List<Unit> items) {
        data = items;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return data.size();
    }


    @Override
    public UnitViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_item_unit, parent, false);
        return new UnitViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UnitViewHolder holder, int position) {
        holder.bind(data.get(position), ctx, callback);
    }


     static class UnitViewHolder extends RecyclerView.ViewHolder {

        private TextView nameTextView;
        private ImageButton deleteImageButton;
        private ImageButton editImageButton;
        private View item;


        public UnitViewHolder(View itemView) {
            super(itemView);
            deleteImageButton = itemView.findViewById(R.id.delete_unit_img_btn);
            editImageButton = itemView.findViewById(R.id.edit_unit_img_btn);
            nameTextView = (TextView) itemView.findViewById(R.id.name_unit_tv);
            item = itemView;

        }

        public void bind(Unit unit, Context ctx, OnItemClick callback) {
            nameTextView.setText(unit.name);
            editImageButton.setOnClickListener(view -> {
                ctx.startActivity(new Intent(ctx, EditUnitActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra(Unit.class.getCanonicalName(), unit.id));
            });
deleteImageButton.setOnClickListener(view -> {
    callback.onClick(getAdapterPosition());
});

        }


    }
}
