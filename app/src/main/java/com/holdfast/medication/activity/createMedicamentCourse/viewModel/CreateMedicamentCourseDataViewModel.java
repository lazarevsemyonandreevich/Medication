package com.holdfast.medication.activity.createMedicamentCourse.viewModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.database.sqlite.SQLiteConstraintException;

import com.holdfast.medication.MedicationApplication;
import com.holdfast.medication.model.dataClass.Medicament;
import com.holdfast.medication.model.dataClass.Reception;
import com.holdfast.medication.model.dataClass.TimeTake;
import com.holdfast.medication.model.dataClass.Unit;
import com.holdfast.medication.notification.NotificationAlarmManager;
import com.holdfast.medication.otherClass.Result;
import com.holdfast.medication.model.service.MedicamentCourseService;
import com.holdfast.medication.model.service.MedicamentService;
import com.holdfast.medication.model.service.UnitService;

import java.util.List;

import javax.inject.Inject;

public class CreateMedicamentCourseDataViewModel extends ViewModel {

    @Inject
    MedicamentCourseService medicamentCourseService;
    @Inject
    MedicamentService medicamentService;
    @Inject
    UnitService unitService;
    @Inject
    NotificationAlarmManager notificationAlarmManager;
    @Inject
    MutableLiveData<Result> result;

    private LiveData<List<Medicament>> medicamentList;
    private LiveData<List<Unit>> unitList;




    public CreateMedicamentCourseDataViewModel() {
        MedicationApplication.getComponent().inject(this);
        medicamentList = medicamentService.getListLiveDataMedicament();
        unitList = unitService.getListLiveDataUnit();

    }

    public LiveData<List<Medicament>> getMedicamentList() {
        return medicamentList;
    }

    public LiveData<List<Unit>> getUnitList() {
        return unitList;
    }


    public LiveData<Result> getResult() {
        return result;
    }


    public void createMedicamentCourse(String name, String note, int eatingType, int eatingTime, TimeTake timeTake, long unitId, long medicamentId, List<Reception> receptionList) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    long medicamentCourseId = medicamentCourseService.add(name, note, eatingType, eatingTime, timeTake, unitId, medicamentId, receptionList);
                    notificationAlarmManager.createRemind(medicamentCourseId);
                    result.postValue(new Result(null, Long.toString(medicamentCourseId)));
                } catch (Exception e) {
                    if (e instanceof SQLiteConstraintException)
                        result.postValue(new Result(new SQLiteConstraintException(), "error"));
                }
            }
        }).start();
    }
}
