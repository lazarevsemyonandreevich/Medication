package com.holdfast.medication.model.dataClass;

import android.app.PendingIntent;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.content.Context;
import android.content.Intent;

import com.holdfast.medication.notification.NotificationReceiver;

@Entity(tableName = "notificationAlarm")
public class NotificationAlarm {
    @PrimaryKey(autoGenerate = true)
    public long id;
    public String message;
    public long medicamentCourseId;
    public int requestCode;
    public int type;

    public NotificationAlarm(long medicamentCourseId, int requestCode, String message, int type) {
        this.medicamentCourseId = medicamentCourseId;
        this.message = message;
        this.requestCode = requestCode;
        this.type = type;
    }


    public static PendingIntent getCreatedPendingIntent(Context context, long medicamentCourseId, int requestCode, String message, int type) {
        Intent intent = new Intent(context, NotificationReceiver.class);
        intent.setAction(Integer.toString((int) medicamentCourseId));
        intent.putExtra("message", message);
        intent.putExtra("medicamentCourseId", medicamentCourseId);
        intent.putExtra("type", type);
        PendingIntent sender = PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return sender;
    }

    public static PendingIntent getDeletedPendingIntent(Context context, NotificationAlarm elem) {
        Intent intent = new Intent(context, NotificationReceiver.class);
        intent.setAction(Integer.toString((int) elem.medicamentCourseId));
        intent.putExtra("message", elem.message);
        intent.putExtra("medicamentCourseId", elem.medicamentCourseId);
        intent.putExtra("type", elem.type);
        PendingIntent sender = PendingIntent.getBroadcast(context, elem.requestCode, intent, PendingIntent.FLAG_NO_CREATE);
        return sender;
    }
}
