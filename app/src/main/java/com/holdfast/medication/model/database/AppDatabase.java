package com.holdfast.medication.model.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;

import com.holdfast.medication.model.dao.MedicamentCourseDao;
import com.holdfast.medication.model.dao.MedicamentDao;
import com.holdfast.medication.model.dao.NotificationAlarmDao;
import com.holdfast.medication.model.dao.ReceptionDao;
import com.holdfast.medication.model.dao.TimeTakeDao;
import com.holdfast.medication.model.dao.UnitDao;
import com.holdfast.medication.model.dataClass.NotificationAlarm;
import com.holdfast.medication.model.dataClass.Medicament;
import com.holdfast.medication.model.dataClass.MedicamentCourse;
import com.holdfast.medication.model.dataClass.Reception;
import com.holdfast.medication.model.dataClass.TimeTake;
import com.holdfast.medication.model.dataClass.Unit;


@android.arch.persistence.room.Database(entities = {Unit.class, MedicamentCourse.class, Medicament.class, TimeTake.class, Reception.class, NotificationAlarm.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract UnitDao unitDao();

    public abstract MedicamentCourseDao medicamentCourseDao();

    public abstract MedicamentDao medicamentDao();

    public abstract TimeTakeDao timeTakeDao();

    public abstract ReceptionDao receptionDao();

    public abstract NotificationAlarmDao notificationAlarmDao();

    public static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(final SupportSQLiteDatabase database) {
            // пример запроса для миграции:
            //   AppDatabase.execSQL("ALTER TABLE Student ADD COLUMN years INTEGER DEFAULT 0 NOT NULL");
        }
    };

}

