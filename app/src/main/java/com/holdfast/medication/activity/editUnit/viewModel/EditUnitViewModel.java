package com.holdfast.medication.activity.editUnit.viewModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.database.sqlite.SQLiteConstraintException;

import com.holdfast.medication.MedicationApplication;
import com.holdfast.medication.model.dataClass.Unit;
import com.holdfast.medication.model.service.UnitService;
import com.holdfast.medication.otherClass.Result;

import javax.inject.Inject;

public class EditUnitViewModel extends ViewModel {

    @Inject
    UnitService unitService;
    @Inject
    MutableLiveData<Result> result;
    private LiveData<Unit> unit;

    public EditUnitViewModel() {
        MedicationApplication.getComponent().inject(this);

    }

    public LiveData<Unit> getUnit(long unitId) {
        if (unit ==null){
            unit = unitService.getLiveDataUnit(unitId);
        }
        return unit;
    }


    public void updateUnit(Unit unit) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    unitService.update(unit);
                    result.postValue(new Result(null, "success"));
                } catch (Exception e) {
                    if (e instanceof SQLiteConstraintException)
                        result.postValue(new Result(e, "already exist"));
                    else result.postValue(new Result(e, null));
                }
            }
        }).start();
    }

    public LiveData<Result> getResult(){
        return result;
    }
}
