package com.holdfast.medication.model.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.holdfast.medication.model.dataClass.Medicament;

import java.util.List;

@Dao
public interface MedicamentDao {
    @Query("SELECT * FROM medicament")
    List<Medicament> getAll();

    @Query("SELECT * FROM medicament")
    LiveData<List<Medicament>> getAllLive();

    @Query("DELETE FROM medicament")
    void deleteAll();

    @Query("SELECT * FROM medicament WHERE id = :id")
    Medicament getById(long id);

    @Query("DELETE FROM medicament WHERE id = :id")
    void deleteById(long id);

    @Query("SELECT * FROM medicament WHERE id = :id")
    LiveData<Medicament> getByIdLive(long id);

    @Query("SELECT * FROM medicament WHERE name = :name")
    Medicament getByName(String name);

    @Insert
    long insert(Medicament medicament);

    @Insert
    long insertWidthReturnId(Medicament medicament);

    @Update
    int update(Medicament medicament);

    @Delete
    int delete(Medicament medicament);
}
