package com.holdfast.medication.activity.main.viewModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.holdfast.medication.MedicationApplication;
import com.holdfast.medication.model.dataClass.Medicament;
import com.holdfast.medication.model.dataClass.Unit;
import com.holdfast.medication.model.service.MedicamentCourseService;
import com.holdfast.medication.model.service.MedicamentService;
import com.holdfast.medication.model.service.UnitService;
import com.holdfast.medication.notification.NotificationAlarmManager;
import com.holdfast.medication.otherClass.MedicamentCoursePOJO;
import com.holdfast.medication.otherClass.Result;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class MainViewModel extends ViewModel {

    @Inject
    NotificationAlarmManager notificationAlarmManager;
    @Inject
    MutableLiveData<Result> result;
    @Inject
    MedicamentCourseService medicamentCourseService;
    @Inject
    MedicamentService medicamentService;
    @Inject
    UnitService unitService;

    private LiveData<List<MedicamentCoursePOJO>> medicamentCourseList;
    private LiveData<List<Medicament>> medicamentList;
    private LiveData<List<Unit>> unitList;


    public MainViewModel() {
        MedicationApplication.getComponent().inject(this);
        medicamentCourseList = medicamentCourseService.getListLiveDataMedicamentCoursePOJO();
        medicamentList = medicamentService.getListLiveDataMedicament();
        unitList = unitService.getListLiveDataUnit();
    }


    public LiveData<List<MedicamentCoursePOJO>> getMedicamentCourseList() {

        return medicamentCourseList;
    }


    public void delUnit(long unitId) {
        unitService.delete(unitId);
        result.postValue(new Result(null, Long.toString(unitId)));
        notificationAlarmManager.restoreReminds();
    }

    public LiveData<List<Medicament>> getMedicamentList() {
        return medicamentList;
    }

    public LiveData<List<Unit>> getUnitList() {
        return unitList;
    }

    public LiveData<Result> getResult() {
        return result;
    }

    public List<MedicamentCoursePOJO> getNotActualityMedicamentCourses(List<MedicamentCoursePOJO> value) {
        List<MedicamentCoursePOJO> actualityCourses = new ArrayList<MedicamentCoursePOJO>();
        for (MedicamentCoursePOJO elem : value) {
            if (!medicamentCourseService.checkActualityMedicamentCourse(elem.timesTake.get(0), elem.receptions))
                actualityCourses.add(elem);
        }
        return actualityCourses;
    }

    public List<MedicamentCoursePOJO> getActualityMedicamentCourses(List<MedicamentCoursePOJO> value) {
        List<MedicamentCoursePOJO> actualityCourses = new ArrayList<MedicamentCoursePOJO>();
        for (MedicamentCoursePOJO elem : value) {
            if (medicamentCourseService.checkActualityMedicamentCourse(elem.timesTake.get(0), elem.receptions))
                actualityCourses.add(elem);
        }
        return actualityCourses;
    }

    public int getRemainingDaysReception(MedicamentCoursePOJO medicamentCoursePOJO) {
        return medicamentCourseService.getRemainingDaysReception(medicamentCoursePOJO);
    }

    public void repeatMedicamentCourse(MedicamentCoursePOJO medicamentCoursePOJO) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                medicamentCourseService.repeatMedicamentCourse(medicamentCoursePOJO);
            }
        }).start();

    }
}
