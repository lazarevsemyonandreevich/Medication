package com.holdfast.medication.otherClass;

public class Result<T> {
       private Throwable error;
       private T result;

        public Result(Throwable error, T result) {
            this.error = error;
            this.result = result;
        }



    public Throwable getError() {
        return error;
    }

    public T getResult() {
        return result;
    }
}

