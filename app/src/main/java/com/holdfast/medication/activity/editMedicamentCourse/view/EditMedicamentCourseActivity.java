package com.holdfast.medication.activity.editMedicamentCourse.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.holdfast.medication.R;
import com.holdfast.medication.activity.createMedicament.view.CreateMedicamentActivity;
import com.holdfast.medication.activity.createUnit.view.CreateUnitActivity;
import com.holdfast.medication.activity.editMedicamentCourse.viewModel.EditMedicamentCourseDataViewModel;
import com.holdfast.medication.activity.editMedicamentCourse.viewModel.EditMedicamentCourseSaveStateViewModel;
import com.holdfast.medication.activity.medicamentCourseInformation.view.MedicamentCourseActivity;
import com.holdfast.medication.otherClass.customView.CustomSpinnerAdapter;
import com.holdfast.medication.otherClass.customView.DateView;
import com.holdfast.medication.model.dataClass.Medicament;
import com.holdfast.medication.model.dataClass.MedicamentCourse;
import com.holdfast.medication.model.dataClass.Reception;
import com.holdfast.medication.model.dataClass.TimeTake;
import com.holdfast.medication.model.dataClass.Unit;
import com.holdfast.medication.otherClass.MedicamentCoursePOJO;
import com.holdfast.medication.otherClass.Result;

import java.util.ArrayList;
import java.util.List;

public class EditMedicamentCourseActivity extends AppCompatActivity {

    private final static int CREATE_MEDICAMENT = 1;
    private final static int CREATE_UNIT = 2;

    long idMedicamentCourse;

    Toolbar toolbar;

    EditMedicamentCourseDataViewModel databaseViewModel;
    EditMedicamentCourseSaveStateViewModel stateViewModel;

    RecyclerView receptionRecyclerView;

    private String addedSpinnerPositionName;//как бы от тебя избавиться

    EditText nameEditText;
    EditText noteEditText;

    DateView startReceptionDateView;
    DateView endReceptionDateView;

    Spinner receptionsDayCountSpinner;
    Spinner unitSpinner;
    Spinner medicamentSpinner;

    RadioGroup eatingTypeRadioGroup;
    RadioButton beforeEatingRadioButton;
    RadioButton whileEatingRadioButton;
    RadioButton afterEatingRadioButton;
    EditText timeEatingEditText;

    LinearLayout eatingTimeLinearLayout;
    LinearLayout lastDateLinearLayout;
    LinearLayout periodsLinearLayout;


    ImageButton addMedicamentImageButton;
    ImageButton addUnitImageButton;

    private boolean manyLoadMedicamentCourseInformation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_medicament_course);
        databaseViewModel = ViewModelProviders.of(this).get(EditMedicamentCourseDataViewModel.class);
        stateViewModel = ViewModelProviders.of(this).get(EditMedicamentCourseSaveStateViewModel.class);

        Intent medicamentCourseIntent = getIntent();
        idMedicamentCourse = medicamentCourseIntent.getLongExtra(MedicamentCourseActivity.class.getCanonicalName(), 0);

        findView();
        setOnClick();
        setView();
        observeViewModel();

    }


    void findView() {
        toolbar = findViewById(R.id.edit_medicament_course_toolbar);
        setSupportActionBar(toolbar);

        nameEditText = findViewById(R.id.edit_medicament_course_name_et);
        timeEatingEditText = findViewById(R.id.time_eating_et);
        noteEditText = findViewById(R.id.edit_medicament_note_et);

        startReceptionDateView = findViewById(R.id.start_reception_date_view);
        endReceptionDateView = findViewById(R.id.end_reception_date_view);


        eatingTypeRadioGroup = findViewById(R.id.eat_time_rg);
        beforeEatingRadioButton = findViewById(R.id.before_eating_rb);
        whileEatingRadioButton = findViewById(R.id.while_eating_rb);
        afterEatingRadioButton = findViewById(R.id.after_eating_rb);


        eatingTimeLinearLayout = findViewById(R.id.eat_time_ll);
        lastDateLinearLayout = findViewById(R.id.last_date_ll);


        medicamentSpinner = findViewById(R.id.medicament_sp);
        unitSpinner = findViewById(R.id.unit_sp);
        receptionsDayCountSpinner = findViewById(R.id.count_receptions_sp);

        receptionRecyclerView = findViewById(R.id.receptions_rv);

        addMedicamentImageButton = findViewById(R.id.add_medicament_ibtn);
        addUnitImageButton = findViewById(R.id.add_unit_ibtn);


    }


    private void setView() {
        setMedicamentSpinner();
        setUnitSpinner();
        setReceptionsDayCountSpinner();
        setReceptionRecyclerView();
        setEatingTypeRadioGroup();
        setStartDate();
        setEndDate();

    }

    private void setStartDate() {
        if (stateViewModel.getSavedState()) {
            startReceptionDateView.setDate(stateViewModel.getStartDateSelectedTime());
        }
    }

    private void setEndDate() {
        if (stateViewModel.getSavedState()) {
            endReceptionDateView.setDate(stateViewModel.getEndDateSelectedTime());
        }
    }

    private void setEatingTypeRadioGroup() {
        if (stateViewModel.getSavedState()) {
            int index = stateViewModel.getEatingTypeRadioGroupIndexSelectedItem();
            RadioButton radioButton = (RadioButton) eatingTypeRadioGroup.getChildAt(index);
            eatingTypeRadioGroup.check(radioButton.getId());

            if (radioButton.getText().toString().equals(getResources().getString(R.string.before_eating_med))
                    || radioButton.getText().toString().equals(getResources().getString(R.string.after_eating_med))) {
                eatingTimeLinearLayout.setVisibility(View.VISIBLE);
            } else eatingTimeLinearLayout.setVisibility(View.GONE);
        }
    }

    private void setMedicamentSpinner() {

        CustomSpinnerAdapter spinnerAdapter = new CustomSpinnerAdapter<>(EditMedicamentCourseActivity.this, android.R.layout.simple_spinner_item, new ArrayList<Object>());
        medicamentSpinner.setAdapter(spinnerAdapter);

        medicamentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    private void setUnitSpinner() {

        CustomSpinnerAdapter spinnerAdapter = new CustomSpinnerAdapter<>(EditMedicamentCourseActivity.this, android.R.layout.simple_spinner_item, new ArrayList<Object>());
        unitSpinner.setAdapter(spinnerAdapter);

        unitSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                ReceptionAdapterRecyclerView adapter = (ReceptionAdapterRecyclerView) receptionRecyclerView.getAdapter();
                Unit unit = new Unit(unitSpinner.getSelectedItem().toString());
                adapter.setUnit(unit);


            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }


    private void setReceptionsDayCountSpinner() {

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, stateViewModel.getDayReceptions());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        receptionsDayCountSpinner.setAdapter(adapter);

        if (stateViewModel.getSavedState())
            receptionsDayCountSpinner.setSelection(stateViewModel.getCountDayReceptionSpinnerSelectedPosition());

        receptionsDayCountSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ReceptionAdapterRecyclerView adapter = (ReceptionAdapterRecyclerView) receptionRecyclerView.getAdapter();
                int receptionCount = Integer.parseInt((String) receptionsDayCountSpinner.getItemAtPosition(position));
                databaseViewModel.updateReceptionList(receptionCount);
                adapter.update();
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setReceptionRecyclerView() {
        receptionRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        ReceptionAdapterRecyclerView adapter = new ReceptionAdapterRecyclerView(EditMedicamentCourseActivity.this);
        receptionRecyclerView.setAdapter(adapter);
    }


    private void setOnClick() {
        addMedicamentImageButton.setOnClickListener(v -> {
            Intent intent = new Intent(EditMedicamentCourseActivity.this, CreateMedicamentActivity.class);
            startActivityForResult(intent, CREATE_MEDICAMENT);
        });

        addUnitImageButton.setOnClickListener(v -> {
            Intent intent = new Intent(EditMedicamentCourseActivity.this, CreateUnitActivity.class);
            startActivityForResult(intent, CREATE_UNIT);
        });

        beforeEatingRadioButton.setOnClickListener(v -> {
            eatingTimeLinearLayout.setVisibility(View.VISIBLE);
        });
        whileEatingRadioButton.setOnClickListener(v -> {
            eatingTimeLinearLayout.setVisibility(View.GONE);
            timeEatingEditText.setText("0");
        });
        afterEatingRadioButton.setOnClickListener(v -> {
            eatingTimeLinearLayout.setVisibility(View.VISIBLE);
        });


    }


    boolean checkField() {

        if (nameEditText.getText().toString().matches(getResources().getString(R.string.check_empty_field_medcourse))) {
            Toast.makeText(this, getResources().getString(R.string.correct_name_medcourse_please), Toast.LENGTH_SHORT).show();
            return false;
        } else if (startReceptionDateView.getDate().after(endReceptionDateView.getDate())) {
            Toast.makeText(EditMedicamentCourseActivity.this, R.string.start_day_after_end_day_exception_medcourse, Toast.LENGTH_SHORT).show();
            return false;
        } else if (medicamentSpinner.getSelectedItem().toString().equals(getResources().getString(R.string.select_medicament_medcourse))) {
            Toast.makeText(this, R.string.select_medicament_please_medcourse, Toast.LENGTH_SHORT).show();
            return false;
        } else if (unitSpinner.getSelectedItem().toString().equals(getResources().getString(R.string.select_unit_medcourse))) {
            Toast.makeText(this, R.string.select_unit_please_medcourse, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }


    void observeViewModel() {

        databaseViewModel.getLDMedicamentList().observe(this, new Observer<List<Medicament>>() {
            @Override
            public void onChanged(@Nullable List<Medicament> value) {

                CustomSpinnerAdapter spinnerAdapter = new CustomSpinnerAdapter(EditMedicamentCourseActivity.this, android.R.layout.simple_spinner_item, value);
                medicamentSpinner.setAdapter(spinnerAdapter);

                if (stateViewModel.getSavedState())
                    medicamentSpinner.setSelection(stateViewModel.getMedicamentSpinnerSelectedPosition());
                selectedAddedItem(medicamentSpinner);

            }
        });


        databaseViewModel.getLDUnitList().observe(this, new Observer<List<Unit>>() {
            @Override
            public void onChanged(@Nullable List<Unit> value) {

                CustomSpinnerAdapter spinnerAdapter = new CustomSpinnerAdapter<>(EditMedicamentCourseActivity.this, android.R.layout.simple_spinner_item, value);
                unitSpinner.setAdapter(spinnerAdapter);

                if (stateViewModel.getSavedState())
                    unitSpinner.setSelection(stateViewModel.getUnitSpinnerSelectedPosition());
                selectedAddedItem(unitSpinner);
            }
        });


        databaseViewModel.getResult().observe(this, new Observer<Result>() {
            @Override
            public void onChanged(@Nullable Result value) {


                if (value.getError() == null) {
                    Toast.makeText(EditMedicamentCourseActivity.this, R.string.saved_changes, Toast.LENGTH_SHORT).show();
                    finish();
                } else if (value.getError() instanceof SQLiteConstraintException) {
                    Toast.makeText(EditMedicamentCourseActivity.this, getResources().getString(R.string.already_exist_medicament_course), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(EditMedicamentCourseActivity.this, value.getError().getMessage(), Toast.LENGTH_SHORT).show();

                }


            }
        });


        databaseViewModel.getLDMedicamentCourse(idMedicamentCourse).observe(this, new Observer<MedicamentCoursePOJO>() {
            @Override
            public void onChanged(@Nullable MedicamentCoursePOJO value) {

                if (!databaseViewModel.isLoad()) {
                    databaseViewModel.setMedicamentCoursePOJO(value);

                    MedicamentCourse medicamentCourse = databaseViewModel.getMedicamentCourse();
                    TimeTake timeTake = databaseViewModel.getTimeTake();
                    List<Reception> receptionList = databaseViewModel.getReceptionList();
                    Medicament medicament = databaseViewModel.getMedicament();
                    Unit unit = databaseViewModel.getUnit();

                    if (!stateViewModel.getSavedState() && !manyLoadMedicamentCourseInformation) {
                        nameEditText.setText(medicamentCourse.name);
                        noteEditText.setText(medicamentCourse.note);
                        setCheckedRadioGroupItem(eatingTypeRadioGroup, medicamentCourse.eatType);
                        timeEatingEditText.setText(Integer.toString(medicamentCourse.eatTime));
                        startReceptionDateView.setDate(timeTake.dayStart);
                        endReceptionDateView.setDate(timeTake.dayEnd);
                        receptionsDayCountSpinner.setSelection(receptionList.size() - 1);

                        ReceptionAdapterRecyclerView recyclerViewAdapter = (ReceptionAdapterRecyclerView) receptionRecyclerView.getAdapter();

                        recyclerViewAdapter.setAdapter(receptionList, unit);
                        receptionRecyclerView.setAdapter(recyclerViewAdapter);

                        CustomSpinnerAdapter unitAdapter = (CustomSpinnerAdapter) unitSpinner.getAdapter();
                        for (int i = 0; i < unitAdapter.getCount(); i++) {
                            if (unitAdapter.getObject(i).toString().equals(unit.name)) {
                                unitSpinner.setSelection(i);
                                break;
                            }
                        }

                        CustomSpinnerAdapter medicamentAdapter = (CustomSpinnerAdapter) medicamentSpinner.getAdapter();
                        for (int i = 0; i < medicamentAdapter.getCount(); i++) {
                            if (medicamentAdapter.getObject(i).toString().equals(medicament.name)) {
                                medicamentSpinner.setSelection(i);
                                break;
                            }
                        }
                    }

                }
            }
        });


    }

    private void setCheckedRadioGroupItem(RadioGroup radioGroup, int selectedItem) {
        int count = radioGroup.getChildCount();
        ArrayList<RadioButton> radioButtonList = new ArrayList<RadioButton>();
        for (int i = 0; i < count; i++) {
            View o = radioGroup.getChildAt(i);
            if (o instanceof RadioButton) {
                radioButtonList.add((RadioButton) o);
            }
        }


        for (RadioButton elem : radioButtonList) {
            if (radioGroup.indexOfChild(elem) == selectedItem) {
                eatingTypeRadioGroup.check(elem.getId());
                eatingTypeRadioGroup.jumpDrawablesToCurrentState();
                if (radioGroup.indexOfChild(elem) == MedicamentCourse.BEFORE_EATING
                        || radioGroup.indexOfChild(elem) == MedicamentCourse.AFTER_EATING) {
                    eatingTimeLinearLayout.setVisibility(View.VISIBLE);
                } else eatingTimeLinearLayout.setVisibility(View.GONE);
                break;
            }

        }
    }


    public void selectedAddedItem(Spinner spinner) {
        ArrayAdapter adapter = (ArrayAdapter) spinner.getAdapter();
        int position = adapter.getPosition(addedSpinnerPositionName);
        if (position != -1) {
            spinner.setSelection(position);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CREATE_MEDICAMENT) {
            if (resultCode == RESULT_OK) {
                addedSpinnerPositionName = data.getStringExtra(CreateMedicamentActivity.class.getCanonicalName());
                manyLoadMedicamentCourseInformation = true;
            }
        } else if (requestCode == CREATE_UNIT) {
            if (resultCode == RESULT_OK) {
                addedSpinnerPositionName = data.getStringExtra(CreateUnitActivity.class.getCanonicalName());
                manyLoadMedicamentCourseInformation = true;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stateViewModel.bundle(
                medicamentSpinner.getSelectedItemPosition(),
                unitSpinner.getSelectedItemPosition(),
                receptionsDayCountSpinner.getSelectedItemPosition(),
                eatingTypeRadioGroup.indexOfChild(findViewById(eatingTypeRadioGroup.getCheckedRadioButtonId())),
                startReceptionDateView.getTime(),
                endReceptionDateView.getTime()
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.complete_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.complete_action) {
            if (checkField()) {
                String name = nameEditText.getText().toString();
                String note = noteEditText.getText().toString();
                int eatingType = eatingTypeRadioGroup.indexOfChild((findViewById(eatingTypeRadioGroup.getCheckedRadioButtonId())));
                int eatingTime = Integer.parseInt(timeEatingEditText.getText().toString().equals("") ? "0" : timeEatingEditText.getText().toString());

                CustomSpinnerAdapter medicamentAdapter = (CustomSpinnerAdapter) medicamentSpinner.getAdapter();
                Medicament medicament = (Medicament) medicamentAdapter.getObject(medicamentSpinner.getSelectedItemPosition());

                CustomSpinnerAdapter unitAdapter = (CustomSpinnerAdapter) unitSpinner.getAdapter();

                Unit unit = (Unit) unitAdapter.getObject(unitSpinner.getSelectedItemPosition());

                TimeTake timeTake = databaseViewModel.getTimeTake();

                timeTake.dayStart = startReceptionDateView.getTime();
                timeTake.dayEnd = endReceptionDateView.getTime();


                MedicamentCourse medicamentCourse = databaseViewModel.getMedicamentCourse();
                medicamentCourse.name = name;
                medicamentCourse.note = note;
                medicamentCourse.eatType = eatingType;

                if (medicamentCourse.eatType == MedicamentCourse.WHILE_EATING) {
                    medicamentCourse.eatTime = 0;
                } else medicamentCourse.eatTime = eatingTime;


                medicamentCourse.unitId = unit.id;
                medicamentCourse.medicamentId = medicament.id;


                databaseViewModel.updateMedicamentCourse(
                        medicamentCourse,
                        timeTake
                );
            }
        }
        return super.onOptionsItemSelected(item);
    }

}
