package com.holdfast.medication.activity.medicamentCourseInformation.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.holdfast.medication.R;
import com.holdfast.medication.activity.editMedicamentCourse.view.EditMedicamentCourseActivity;
import com.holdfast.medication.activity.medicamentCourseInformation.viewModel.MedicamentCourseViewModel;
import com.holdfast.medication.model.dataClass.Medicament;
import com.holdfast.medication.model.dataClass.MedicamentCourse;
import com.holdfast.medication.model.dataClass.Reception;
import com.holdfast.medication.model.dataClass.TimeTake;
import com.holdfast.medication.model.dataClass.Unit;
import com.holdfast.medication.otherClass.MedicamentCoursePOJO;
import com.holdfast.medication.otherClass.Result;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class MedicamentCourseActivity extends AppCompatActivity {

    MedicamentCourseViewModel viewModel;


    Toolbar toolbar;

    TextView medicamentTextView;
    TextView dayStartTextView;
    TextView dayEndTextView;
    TextView eatTypeTextView;
    TextView noteTextView;
    TextView eatingTimeTextView;

    RecyclerView receptionRecyclerView;

    LinearLayout eatingTimeLinearLayout;

    long MedicamentCourseId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicament_course_information);

        setView();

        Intent medicamentCourseIntent = getIntent();
        MedicamentCourseId = medicamentCourseIntent.getLongExtra(MedicamentCourse.class.getCanonicalName(), 0);

        viewModel = ViewModelProviders.of(this).get(MedicamentCourseViewModel.class);

        observeViewModel();
    }

    private void setView() {
        toolbar = findViewById(R.id.medicament_course_toolbar);
        setSupportActionBar(toolbar);
        medicamentTextView = findViewById(R.id.medicament_tv);
        dayStartTextView = findViewById(R.id.day_start_tv);
        dayEndTextView = findViewById(R.id.day_end_tv);
        eatTypeTextView = findViewById(R.id.eating_type_tv);
        eatingTimeTextView = findViewById(R.id.time_eating_tv);
        noteTextView = findViewById(R.id.note_tv);

        eatingTimeLinearLayout = findViewById(R.id.eat_time_ll);


        setReceptionRecyclerView();
    }

    private void setReceptionRecyclerView() {
        receptionRecyclerView = findViewById(R.id.receptions_rv);
        receptionRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        receptionRecyclerView.setAdapter(new ReceptionAdapter(this));
        receptionRecyclerView.setNestedScrollingEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.information_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void observeViewModel() {
        viewModel.getResult().observe(this, new Observer<Result>() {
            @Override
            public void onChanged(@Nullable Result value) {
                if (value.getError() == null) {
                    finish();
                } else {
                    if (value.getError() instanceof SQLiteConstraintException) {
                    }
                }
            }
        });

        viewModel.getMedicamentCourse(MedicamentCourseId).observe(this, new Observer<MedicamentCoursePOJO>() {
            @Override
            public void onChanged(@Nullable MedicamentCoursePOJO value) {

                    viewModel.setMedicamentCoursePOJO(value);

                MedicamentCourse medicamentCourse = viewModel.getMedicamentCourse();
                TimeTake timeTake = viewModel.getTimeTake();
                List<Reception> receptionList = viewModel.getReceptionList();
                Medicament medicament = viewModel.getMedicament();
                Unit unit = viewModel.getUnit();



                    if (medicamentCourse.eatType == MedicamentCourse.BEFORE_EATING
                            || medicamentCourse.eatType == MedicamentCourse.AFTER_EATING) {
                        eatingTimeLinearLayout.setVisibility(View.VISIBLE);
                        eatingTimeTextView.setText(Integer.toString(medicamentCourse.eatTime));
                    } else eatingTimeLinearLayout.setVisibility(View.GONE);

                    toolbar.setTitle(medicamentCourse.name);
                    medicamentTextView.setText(medicament.name);
                    String eatType = new String();
                    switch (medicamentCourse.eatType) {
                        case (0):
                            eatType = "До еды";
                            break;
                        case (1):
                            eatType = "Во время еды";
                            break;
                        case (2):
                            eatType = "После еды";
                            break;
                        default:
                            break;
                    }
                    eatTypeTextView.setText(eatType);
                    dayStartTextView.setText(new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(timeTake.dayStart));
                    dayEndTextView.setText(new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(timeTake.dayEnd));
                    noteTextView.setText(medicamentCourse.note);
                    ((ReceptionAdapter) receptionRecyclerView.getAdapter()).setData(receptionList,unit);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.edit_action) {
            Intent intent = new Intent(this, EditMedicamentCourseActivity.class);
            intent.putExtra(MedicamentCourseActivity.class.getCanonicalName(), MedicamentCourseId);
            startActivity(intent);
        }

        if (item.getItemId() == R.id.delete_action) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Удаление")
                    .setMessage("Вы уверены, что хотите удалить курс приёма медикамента?")
                    .setCancelable(false)
                    .setPositiveButton("Принять", (dialog, which) -> viewModel.delMedicamentCourse(MedicamentCourseId))
                    .setNegativeButton("Отмена", (dialog, id) -> dialog.cancel());
            AlertDialog alert = builder.create();
            alert.show();
        }

        return super.onOptionsItemSelected(item);
    }
}
