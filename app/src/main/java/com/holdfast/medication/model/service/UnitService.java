package com.holdfast.medication.model.service;

import android.arch.lifecycle.LiveData;

import com.holdfast.medication.MedicationApplication;
import com.holdfast.medication.model.dataClass.Unit;
import com.holdfast.medication.model.database.AppDatabase;

import java.util.List;

import javax.inject.Inject;

public class UnitService {

    @Inject
    AppDatabase database;

    public UnitService() {
        MedicationApplication.getComponent().inject(this);
    }

    public LiveData<Unit> getLiveDataUnit(long unitId) {
        return database.unitDao().getByIdLive(unitId);
    }

    public LiveData<List<Unit>> getListLiveDataUnit() {
        return database.unitDao().getAllLive();
    }

    public long add(Unit unit) {
        return database.unitDao().insert(unit);
    }

    public void update(Unit unit){
        database.unitDao().update(unit);
    }

    public void delete(long unitId){
        database.unitDao().deleteById(unitId);
    }
}
