package com.holdfast.medication.activity.main.view;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.holdfast.medication.R;
import com.holdfast.medication.MedicationApplication;
import com.holdfast.medication.model.database.AppDatabase;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;

    @Inject
    AppDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MedicationApplication.getComponent().inject(this);
        setView();
        firstStartAction();
    }

    private void setView() {
        setNavigationDrawer();
        setStartFragment();
    }

    private void setStartFragment() {
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fr_place);
        if ((fragment == null)) {
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.fr_place, new FragmentMedicamentCourse());
            ft.commit();
        }

    }


    private void setNavigationDrawer() {
        drawerLayout = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    // set item as selected to persist highlight
                    menuItem.setChecked(true);
                    // close drawer when item is tapped
                    drawerLayout.closeDrawers();

                    // Add code here to update the UI based on the item selected
                    // For example, swap UI fragments here
                    selectNavigationDrawerItem(menuItem.getItemId());

                    return true;
                });
    }

    private void selectNavigationDrawerItem(int itemId) {
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fr_place);
        FragmentTransaction ft = fm.beginTransaction();
        switch (itemId) {

//            case (R.id.nav_treatment_course_list):
//                if (!(fragment instanceof FragmentTreatmentCourse)) {
//                    ft.replace(R.id.fr_place, new FragmentTreatmentCourse());
//                    ft.commit();
//                }
//                break;

            case (R.id.nav_medicament_course_list):
                if (!(fragment instanceof FragmentMedicamentCourse)) {
                    ft.replace(R.id.fr_place, new FragmentMedicamentCourse());
                    ft.commit();
                }
                break;

            case (R.id.nav_medicament_list):
                if (!(fragment instanceof FragmentMedicament)) {
                    ft.replace(R.id.fr_place, new FragmentMedicament());
                    ft.commit();
                }
                break;

            case (R.id.nav_unit_list):
                if (!(fragment instanceof FragmentUnit)) {
                    ft.replace(R.id.fr_place, new FragmentUnit());
                    ft.commit();
                }
                break;

            case (R.id.nav_medicament_course_archive_list):
                if(!(fragment instanceof FragmentMedicamentCourseArchive)){
                    ft.replace(R.id.fr_place, new FragmentMedicamentCourseArchive());
                    ft.commit();
                }
                break;

            case (R.id.nav_program):
                if(!(fragment instanceof FragmentAuthor)){
                    ft.replace(R.id.fr_place, new FragmentAuthor());
                    ft.commit();
                }
                break;
            default:
                break;
        }
    }


    private void firstStartAction() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isAgain = sp.getBoolean("isAgain", false);

        if (!isAgain) {
            Toast.makeText(this, getResources().getString(R.string.welcome), Toast.LENGTH_SHORT).show();
        }

        SharedPreferences.Editor e = sp.edit();
        e.putBoolean("isAgain", true);
        e.apply();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
