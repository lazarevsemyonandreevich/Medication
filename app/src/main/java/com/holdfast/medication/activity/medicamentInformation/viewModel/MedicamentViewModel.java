package com.holdfast.medication.activity.medicamentInformation.viewModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.holdfast.medication.MedicationApplication;
import com.holdfast.medication.model.dataClass.Medicament;
import com.holdfast.medication.model.service.MedicamentService;
import com.holdfast.medication.notification.NotificationAlarmManager;
import com.holdfast.medication.otherClass.Result;

import javax.inject.Inject;

public class MedicamentViewModel extends ViewModel {

    @Inject
    MedicamentService medicamentService;
    @Inject
    NotificationAlarmManager notificationAlarmManager;
    @Inject
    MutableLiveData<Result> result;

    public MedicamentViewModel() {
        MedicationApplication.getComponent().inject(this);
    }

    public LiveData<Medicament> getMedicament(long medicamentId) {
        return medicamentService.getLiveDataMedicament(medicamentId);
    }

    public void delMedicament(long medicamentId){
        new Thread(new Runnable() {
            @Override
            public void run() {
                medicamentService.delete(medicamentId);
                result.postValue(new Result(null, Long.toString(medicamentId)));
                notificationAlarmManager.restoreReminds();
            }
        }).start();
    }

    public LiveData<Result> getResult(){
        return result;
    }
}
