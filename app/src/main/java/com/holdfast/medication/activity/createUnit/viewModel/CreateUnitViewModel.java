package com.holdfast.medication.activity.createUnit.viewModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.database.sqlite.SQLiteConstraintException;

import com.holdfast.medication.MedicationApplication;
import com.holdfast.medication.model.dataClass.Unit;
import com.holdfast.medication.otherClass.Result;
import com.holdfast.medication.model.service.UnitService;

import javax.inject.Inject;

public class CreateUnitViewModel extends ViewModel {

    @Inject
    UnitService unitService;
    @Inject
    MutableLiveData<Result> result;

    public CreateUnitViewModel() {
        MedicationApplication.getComponent().inject(this);
    }

    public void createUnit(Unit unit) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    unitService.add(unit);
                    result.postValue(new Result(null, "success"));
                } catch (SQLiteConstraintException e) {
                    result.postValue(new Result(new SQLiteConstraintException(), "error"));
                }
            }
        }).start();
    }

    public LiveData<Result> getResult() {
        return result;
    }
}
