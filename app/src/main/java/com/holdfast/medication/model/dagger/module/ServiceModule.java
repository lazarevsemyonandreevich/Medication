package com.holdfast.medication.model.dagger.module;

import com.holdfast.medication.model.service.MedicamentCourseService;
import com.holdfast.medication.model.service.MedicamentService;
import com.holdfast.medication.model.service.UnitService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


    @Module()
    public class ServiceModule {


        @Singleton
        @Provides
        MedicamentCourseService provideMedicamentCourseService() {
            return new MedicamentCourseService();
        }

        @Singleton
        @Provides
        MedicamentService provideMedicamentService() {
            return new MedicamentService();
        }

        @Singleton
        @Provides
        UnitService provideUnitService() {
            return new UnitService();
        }

    }

