package com.holdfast.medication.otherClass.customView;

import android.app.DatePickerDialog;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.DatePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateView extends android.support.v7.widget.AppCompatTextView implements View.OnClickListener {

    private Context context;

    private Calendar date;
    private Calendar minDate;
    private Calendar maxDate;

    private String dateFormat;


    private DatePickerDialog.OnDateSetListener datePicker = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            date.set(year, monthOfYear, dayOfMonth);
            setText(new SimpleDateFormat(dateFormat, Locale.getDefault()).format(date.getTimeInMillis()));
        }
    };


    public DateView(Context ctx, AttributeSet attrs) {
        super(ctx, attrs);
        this.context = ctx;

        this.date = createCalendarInstance();
        this.minDate = createCalendarInstance();
        this.maxDate = createCalendarInstance();
        minDate.set(1970, 0, 1, 0, 0, 0);
        maxDate.set(3000, 0, 0, 0, 0, 0);
        dateFormat = "dd/MM/yy";
        setText(new SimpleDateFormat(dateFormat, Locale.getDefault()).format(date.getTimeInMillis()));
        setOnClickListener(this);
    }

    private Calendar createCalendarInstance() {
        Calendar currentDate = Calendar.getInstance();
        currentDate.set(Calendar.HOUR_OF_DAY, 0);
        currentDate.set(Calendar.MINUTE, 0);
        currentDate.set(Calendar.SECOND, 0);
        currentDate.set(Calendar.MILLISECOND, 0);
        return currentDate;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public long getMinDate() {
        return minDate.getTimeInMillis();
    }

    public void setMinDate(long minDate) {
        this.minDate.setTimeInMillis(minDate);
    }

    public long getMaxDate() {
        return maxDate.getTimeInMillis();
    }

    public void setMaxDate(long maxDate) {
        this.maxDate.setTimeInMillis(maxDate);
    }


    public long getTime() {
        return date.getTimeInMillis();
    }

    public Date getDate() {
        return date.getTime();
    }


    @Override
    public void onClick(View v) {
        DatePickerDialog dpd = new DatePickerDialog(context, datePicker,
                date.get(Calendar.YEAR),
                date.get(Calendar.MONTH),
                date.get(Calendar.DAY_OF_MONTH));
        dpd.getDatePicker().setMinDate(minDate.getTimeInMillis());
        dpd.getDatePicker().setMaxDate(maxDate.getTimeInMillis());
        dpd.show();
    }


    public void setDate(long time) {
        date.setTimeInMillis(time);
        setText(new SimpleDateFormat(dateFormat, Locale.getDefault()).format(date.getTimeInMillis()));
    }
}
