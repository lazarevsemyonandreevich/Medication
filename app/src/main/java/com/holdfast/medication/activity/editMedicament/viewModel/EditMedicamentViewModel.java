package com.holdfast.medication.activity.editMedicament.viewModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.database.sqlite.SQLiteConstraintException;

import com.holdfast.medication.MedicationApplication;
import com.holdfast.medication.model.dataClass.Medicament;
import com.holdfast.medication.otherClass.Result;
import com.holdfast.medication.model.service.MedicamentService;

import javax.inject.Inject;

public class EditMedicamentViewModel extends ViewModel {

    @Inject
    MedicamentService medicamentService;
    @Inject
    MutableLiveData<Result> result;

    public EditMedicamentViewModel() {
        MedicationApplication.getComponent().inject(this);
    }

    public void updateMedicament(Medicament medicament){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    medicamentService.update(medicament);
                    result.postValue(new Result(null, "success"));
                } catch (Exception e) {
                    if (e instanceof SQLiteConstraintException)
                        result.postValue(new Result(e, "already exist"));
                    else result.postValue(new Result(e, null));
                }
            }
        }).start();
    }

    public LiveData<Medicament> getMedicament(long medicamentId) {
        return medicamentService.getLiveDataMedicament(medicamentId);
    }

    public LiveData<Result> getResult() {
        return result;
    }
}
