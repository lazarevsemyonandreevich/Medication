package com.holdfast.medication.model.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.holdfast.medication.model.dataClass.NotificationAlarm;

import java.util.List;

@Dao
public interface NotificationAlarmDao {
    @Query("SELECT * FROM NotificationAlarm")
    List<NotificationAlarm> getAll();

    @Query("DELETE FROM NotificationAlarm")
    void deleteAll();

    @Query("SELECT * FROM NotificationAlarm WHERE id = :id")
    NotificationAlarm getById(long id);

    @Query("SELECT * FROM NotificationAlarm WHERE medicamentCourseId = :medicamentCourseId")
    List<NotificationAlarm> getByMedicamentCourseId(long medicamentCourseId);

    @Insert
    void insert(NotificationAlarm notificationAlarm);

    @Insert
    long insertWidthReturnId(NotificationAlarm notificationAlarm);

    @Update
    int update(NotificationAlarm notificationAlarm);

    @Delete
    void delete(NotificationAlarm notificationAlarm);
}
