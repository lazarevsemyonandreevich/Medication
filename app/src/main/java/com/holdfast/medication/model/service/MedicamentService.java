package com.holdfast.medication.model.service;

import android.arch.lifecycle.LiveData;

import com.holdfast.medication.MedicationApplication;
import com.holdfast.medication.model.dataClass.Medicament;
import com.holdfast.medication.model.database.AppDatabase;

import java.util.List;

import javax.inject.Inject;

public class MedicamentService {

    @Inject
    AppDatabase database;

    public MedicamentService() {
        MedicationApplication.getComponent().inject(this);
    }

    public LiveData<List<Medicament>> getListLiveDataMedicament() {
        return database.medicamentDao().getAllLive();
    }

    public LiveData<Medicament> getLiveDataMedicament(long medicamentCourseId) {
        return database.medicamentDao().getByIdLive(medicamentCourseId);
    }

    public long add(Medicament medicament) {
         return database.medicamentDao().insert(medicament);
    }

    public void update(Medicament medicament){
        database.medicamentDao().update(medicament);
    }

    public void delete(long medicamentId){
        database.medicamentDao().deleteById(medicamentId);
    }
}
