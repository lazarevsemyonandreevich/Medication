package com.holdfast.medication.model.dataClass;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import java.util.Calendar;

@Entity(tableName = "unit",
        indices = {@Index(value = {"name"}, unique = true)})
public class Unit{

    @PrimaryKey(autoGenerate = true)
    public long id;
    public String name;
    @ColumnInfo(name = "date_create")
    public long dateCreate;


    public Unit(String name) {
        this.name = name;
        dateCreate= Calendar.getInstance().getTimeInMillis();
    }

    @Override
    public String toString() {
        return name;
    }
}
