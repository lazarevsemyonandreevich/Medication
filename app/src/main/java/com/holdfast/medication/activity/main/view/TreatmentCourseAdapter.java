//package com.holdfast.medication.activity.main.view;
//
//import android.content.Context;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import com.holdfast.medication.R;
//import com.holdfast.medication.activity.main.viewModel.MainViewModel;
//import com.holdfast.medication.model.dataClass.TreatmentCourse;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class TreatmentCourseAdapter extends RecyclerView.Adapter<TreatmentCourseAdapter.TreatmentAdapterViewHolder> {
//
//    private List<TreatmentCourse> data;
//    private Context ctx;
//    private MainViewModel viewModel;
//
//    public TreatmentCourseAdapter(Context ctx, MainViewModel viewModel) {
//        this.data = new ArrayList<>();
//        this.ctx = ctx;
//        this.viewModel=viewModel;
//    }
//
//
//    public List<TreatmentCourse> getData() {
//        return data;
//    }
//
//    public TreatmentCourse getItem(int position) {
//        return data.get(position);
//    }
//
//
//    public void addItem(TreatmentCourse item) {
//        data.add(item);
//        notifyDataSetChanged();
//    }
//
//    public void setItems(List<TreatmentCourse> items) {
//        data = items;
//        notifyDataSetChanged();
//    }
//
//
//    @Override
//    public int getItemCount() {
//        return data.size();
//    }
//
//
//    @Override
//    public TreatmentAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.recyclerview_item_medicament_course, parent, false);
//        return new TreatmentAdapterViewHolder(view, viewModel);
//    }
//
//    @Override
//    public void onBindViewHolder(TreatmentAdapterViewHolder holder, int position) {
//        holder.bind(data.get(position), ctx);
//    }
//
//
//    public static class TreatmentAdapterViewHolder extends RecyclerView.ViewHolder {
//
////        @Inject
////        NotificationAlarmManager notificationAlarmManager;
//
//        private TextView nameTextView;
//        private TextView noteTextView;
//        //private TextView dayTakeTextView;
//        private View item;
//        private MainViewModel viewModel;
//
//
//        public TreatmentAdapterViewHolder(View itemView, MainViewModel viewModel) {
//            super(itemView);
//           // MedicationApplication.getComponent().inject(this);
//            nameTextView = (TextView) itemView.findViewById(R.id.name_treatment_course_tv);
//            noteTextView = (TextView) itemView.findViewById(R.id.note_treatment_course_tv);
//            //dayTakeTextView = itemView.findViewById(R.id.day_take_tv);
//            item = itemView;
//            this.viewModel=viewModel;
//
//        }
//
//        public void bind(TreatmentCourse treatmentCourse, Context ctx) {
//            nameTextView.setText(treatmentCourse.name);
//            noteTextView.setText(treatmentCourse.note);
//          //  String days =String.format(" %d", viewModel.getRemainingDaysReception(treatmentCourse));
//           // dayTakeTextView.setText(days);
//
//
////            item.setOnClickListener(v -> {
////                ctx.startActivity(new Intent(ctx, MedicamentCourseActivity.class)
////                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
////                        .putExtra(MedicamentCourse.class.getCanonicalName(),
////                                treatmentCourse.medicamentCourse.id)
////                );
////            });
//
//        }
//
//
//    }
//}
