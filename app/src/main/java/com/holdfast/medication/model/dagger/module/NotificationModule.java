package com.holdfast.medication.model.dagger.module;

import com.holdfast.medication.notification.NotificationAlarmManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class NotificationModule {

    @Singleton
    @Provides
    NotificationAlarmManager provideNotificationAlarmManager(){
        return NotificationAlarmManager.getInstance();
    }
}
