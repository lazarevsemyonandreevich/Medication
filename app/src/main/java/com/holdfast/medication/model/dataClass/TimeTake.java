package com.holdfast.medication.model.dataClass;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Calendar;

@Entity(
        tableName = "time_take",
        foreignKeys = @ForeignKey(entity = MedicamentCourse.class, parentColumns = "id", childColumns = "medicament_course_id", onDelete = ForeignKey.CASCADE))
public class TimeTake {
    @PrimaryKey(autoGenerate = true)
    public long id;
    @ColumnInfo(name = "day_start")
    public long dayStart;
    @ColumnInfo(name = "day_end")
    public long dayEnd;
    @NonNull
    @ColumnInfo(name = "medicament_course_id", index = true)
    public long medicamentCourseId;

    @Ignore
    public TimeTake(long dayStart, long dayEnd) {
        this.dayStart = dayStart;
        this.dayEnd = dayEnd;
    }


    public TimeTake(long dayStart, long dayEnd, long medicamentCourseId) {
        this.dayStart = dayStart;
        this.dayEnd = dayEnd;
        this.medicamentCourseId = medicamentCourseId;
    }


    public int getCountDay() {
        Calendar currentDate = Calendar.getInstance();
        currentDate.set(Calendar.HOUR_OF_DAY, 0);
        currentDate.set(Calendar.MINUTE, 0);
        currentDate.set(Calendar.SECOND, 0);

        final int partTime = 1;
        int countDay = (int) ((dayEnd - dayStart) / (1000 * 60 * 60 * 24));
        return countDay + partTime;
    }
}
