package com.holdfast.medication.activity.medicamentCourseInformation.viewModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.holdfast.medication.MedicationApplication;
import com.holdfast.medication.model.dataClass.Medicament;
import com.holdfast.medication.model.dataClass.MedicamentCourse;
import com.holdfast.medication.model.dataClass.Reception;
import com.holdfast.medication.model.dataClass.TimeTake;
import com.holdfast.medication.model.dataClass.Unit;
import com.holdfast.medication.model.service.MedicamentCourseService;
import com.holdfast.medication.notification.NotificationAlarmManager;
import com.holdfast.medication.otherClass.MedicamentCoursePOJO;
import com.holdfast.medication.otherClass.Result;

import java.util.List;

import javax.inject.Inject;

public class MedicamentCourseViewModel extends ViewModel {

    private boolean load;

    private List<Reception> receptionList;
    private Medicament medicament;
    private TimeTake timeTake;
    private Unit unit;
    private MedicamentCourse medicamentCourse;

    @Inject
    MedicamentCourseService medicamentCourseService;
    @Inject
    NotificationAlarmManager notificationAlarmManager;
    @Inject
    MutableLiveData<Result> result;

    public MedicamentCourseViewModel() {
        MedicationApplication.getComponent().inject(this);
    }

    public LiveData<MedicamentCoursePOJO> getMedicamentCourse(long medicamentCourseId) {

        return medicamentCourseService.getLiveDataMedicamentCoursePOJO(medicamentCourseId);
    }

    public void delMedicamentCourse(long medicamentCourseId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                medicamentCourseService.delete(medicamentCourseId);
                notificationAlarmManager.deleteRemind(medicamentCourseId);
                result.postValue(new Result(null, Long.toString(medicamentCourseId)));
            }
        }).start();
    }

    public LiveData<Result> getResult() {
        return result;
    }

    public List<Reception> getReceptionList() {
        return receptionList;
    }

    public Medicament getMedicament() {
        return medicament;
    }


    public TimeTake getTimeTake() {
        return timeTake;
    }


    public Unit getUnit() {
        return unit;
    }


    public MedicamentCourse getMedicamentCourse() {
        return medicamentCourse;
    }

    public void setMedicamentCoursePOJO(MedicamentCoursePOJO medicamentCoursePOJO) {
        this.medicamentCourse = medicamentCoursePOJO.medicamentCourse;
        this.medicament = medicamentCoursePOJO.medicaments.get(0);
        this.receptionList = medicamentCoursePOJO.receptions;
        this.timeTake = medicamentCoursePOJO.timesTake.get(0);
        this.unit = medicamentCoursePOJO.units.get(0);
        load = true;
    }

    public boolean isLoad() {
        return load;
    }
}


