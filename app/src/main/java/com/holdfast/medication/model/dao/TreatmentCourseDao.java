package com.holdfast.medication.model.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.holdfast.medication.model.dataClass.TreatmentCourse;

import java.util.List;

public interface TreatmentCourseDao{
    @Query("SELECT * FROM treatment_course")
    List<TreatmentCourse> getAll();

    @Query("SELECT * FROM treatment_course")
    LiveData<List<TreatmentCourse>> getAllLive();

    @Query("DELETE FROM treatment_course")
    void deleteAll();

    @Query("SELECT * FROM treatment_course WHERE id = :id")
    TreatmentCourse getById(long id);

    @Query("DELETE FROM treatment_course WHERE id = :id")
    void deleteById(long id);

    @Query("SELECT * FROM treatment_course WHERE id = :id")
    LiveData<TreatmentCourse> getByIdLive(long id);

    @Query("SELECT * FROM treatment_course WHERE name = :name")
    TreatmentCourse getByName(String name);

    @Insert
    long insert(TreatmentCourse treatmentCourse);

    @Insert
    long insertWidthReturnId(TreatmentCourse treatmentCourse);

    @Update
    int update(TreatmentCourse treatmentCourse);

    @Delete
    int delete(TreatmentCourse treatmentCourse);
}
