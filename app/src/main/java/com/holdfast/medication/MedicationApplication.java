package com.holdfast.medication;

import android.app.Application;

import com.holdfast.medication.model.dagger.AppComponent;
import com.holdfast.medication.model.dagger.DaggerAppComponent;
import com.holdfast.medication.model.dagger.module.ContextModule;
import com.holdfast.medication.model.dagger.module.OtherClassModule;
import com.holdfast.medication.model.dagger.module.RoomModule;
import com.holdfast.medication.model.dagger.module.ServiceModule;

public class MedicationApplication extends Application {

    private static AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerAppComponent.builder()
                .contextModule(new ContextModule(this))
                .roomModule(new RoomModule(this))
                .serviceModule(new ServiceModule())
                .otherClassModule(new OtherClassModule())
                .build();
    }


    public static AppComponent getComponent() {
        return component;
    }
}
