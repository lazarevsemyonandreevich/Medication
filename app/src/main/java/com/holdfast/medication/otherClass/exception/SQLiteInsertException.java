package com.holdfast.medication.otherClass.exception;

import android.database.sqlite.SQLiteConstraintException;

public class SQLiteInsertException extends SQLiteConstraintException{

        public SQLiteInsertException() {}

        public SQLiteInsertException(String error) {
            super(error);
        }


}
