package com.holdfast.medication.activity.createUnit.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.holdfast.medication.R;
import com.holdfast.medication.activity.createUnit.viewModel.CreateUnitViewModel;
import com.holdfast.medication.model.dataClass.Unit;
import com.holdfast.medication.otherClass.Result;

public class CreateUnitActivity extends AppCompatActivity {

    Toolbar toolbar;
    EditText nameEditText;

    CreateUnitViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_unit);

        findView();


        viewModel = ViewModelProviders.of(this).get(CreateUnitViewModel.class);
        observeViewModel(viewModel);
    }

    void findView() {
        toolbar = findViewById(R.id.edit_unit_toolbar);
        setSupportActionBar(toolbar);
        nameEditText = findViewById(R.id.unit_name_et);

    }


    private void observeViewModel(CreateUnitViewModel viewModel) {

        viewModel.getResult().observe(this, new Observer<Result>() {
            @Override
            public void onChanged(@Nullable Result value) {
                if (value.getResult().equals("success")) {
                    Intent intent = new Intent();
                    intent.putExtra(CreateUnitActivity.class.getCanonicalName(), nameEditText.getText().toString());
                    setResult(RESULT_OK, intent);
                    finish();
                }

                if (value.getResult().equals("error")) {
                    if (value.getError() instanceof SQLiteConstraintException){
                        Toast.makeText(CreateUnitActivity.this, getResources().getString(R.string.already_exist_unit), Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.complete_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.complete_action) {
            if (checkField()) {
                viewModel.createUnit(new Unit(nameEditText.getText().toString()));
            }
        }
        return super.onOptionsItemSelected(item);
    }

    boolean checkField() {
        if (nameEditText.getText().toString().matches("\\W*")) {
            Toast.makeText(this, getResources().getString(R.string.correct_name_please_unit), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


}
