//package com.holdfast.medication.activity.main.view;
//
//import android.arch.lifecycle.Observer;
//import android.arch.lifecycle.ViewModelProviders;
//import android.content.Context;
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.support.v7.widget.Toolbar;
//import android.view.LayoutInflater;
//import android.view.Menu;
//import android.view.MenuInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewGroup;
//
//import com.holdfast.medication.MedicationApplication;
//import com.holdfast.medication.R;
//import com.holdfast.medication.activity.createMedicamentCourse.view.CreateMedicamentCourseActivity;
//import com.holdfast.medication.activity.main.viewModel.MainViewModel;
//import com.holdfast.medication.model.dataClass.TreatmentCourse;
//import com.holdfast.medication.otherClass.MedicamentCoursePOJO;
//
//import java.util.List;
//
//import javax.inject.Inject;
//
//public class FragmentTreatmentCourse extends Fragment {
//
//
//    @Inject
//    Context context;
//
//    private RecyclerView recyclerView;
//    private MainViewModel viewModel;
//    private TreatmentCourseAdapter adapter;
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        MedicationApplication.getComponent().inject(this);
//
//        View rootView = inflater.inflate(R.layout.fragment_medcourse, null);
//
//        setHasOptionsMenu(true);//is necessary for onCreateOptionsMenu
//
//        setToolbar(getResources().getString(R.string.treatment_list));
//
//        recyclerView = rootView.findViewById(R.id.treatment_course_rv);
//
//        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
//
//        setTreatmentCourseRecyclerView();
//
//        observeViewModel(viewModel);
//        return rootView;
//    }
//
//
//
//    void setToolbar( String title) {
//        Toolbar toolbar = getActivity().findViewById(R.id.main_toolbar);
//        toolbar.setTitle(title);
//        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.round_reorder_white_36);
//    }
//
//    private void setTreatmentCourseRecyclerView() {
//        adapter = new TreatmentCourseAdapter(context,viewModel);
//        recyclerView.setLayoutManager(new LinearLayoutManager(context));
//        recyclerView.setAdapter(adapter);
//    }
//
//
//    void observeViewModel(MainViewModel viewModel) {
//
//        viewModel.getMedicamentCourseList().observe(this, new Observer<List<TreatmentCourse>>() {
//            @Override
//            public void onChanged(@Nullable List<TreatmentCourse> value) {
//                adapter.setItems(viewModel.getActualityMedicamentCourses(value));
//                recyclerView.setAdapter(adapter);
//            }
//        });
//    }
//
//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.preview_list_menu, menu);
//        super.onCreateOptionsMenu(menu, inflater);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == R.id.add_action) {
//            startActivity(new Intent(context, CreateMedicamentCourseActivity.class));
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
//}
