package com.holdfast.medication.model.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.holdfast.medication.model.dataClass.TimeTake;

import java.util.List;

@Dao
public interface TimeTakeDao {
    @Query("SELECT * FROM time_take")
    LiveData<List<TimeTake>> getAllLive();

    @Query("SELECT * FROM time_take")
    List<TimeTake> getAll();

    @Query("DELETE FROM time_take")
    void deleteAll();

    @Query("SELECT * FROM time_take WHERE id = :id")
    TimeTake getById(long id);

    @Query("SELECT * FROM time_take WHERE medicament_course_id = :medicamentCourseId")
    TimeTake getByMedicamentCourseId(long medicamentCourseId);

    @Insert
    void insert(TimeTake timeTake);

    @Insert
    long insertWidthReturnId(TimeTake timeTake);

    @Update
    int update(TimeTake timeTake);

    @Delete
    void delete(TimeTake timeTake);
}
