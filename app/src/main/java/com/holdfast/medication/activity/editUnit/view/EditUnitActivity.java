package com.holdfast.medication.activity.editUnit.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.holdfast.medication.R;
import com.holdfast.medication.activity.editUnit.viewModel.EditUnitViewModel;
import com.holdfast.medication.model.dataClass.Unit;
import com.holdfast.medication.otherClass.Result;

public class EditUnitActivity extends AppCompatActivity {

    Toolbar toolbar;
    EditText nameEditText;

    EditUnitViewModel viewModel;

    private long unitId;
    private Unit unit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_unit);

        findView();

        Intent unitIntent = getIntent();
        unitId = unitIntent.getLongExtra(Unit.class.getCanonicalName(), 0);
        viewModel = ViewModelProviders.of(this).get(EditUnitViewModel.class);
        observeViewModel(viewModel);
    }

    void findView() {
        toolbar = findViewById(R.id.edit_unit_toolbar);
        setSupportActionBar(toolbar);
        nameEditText = findViewById(R.id.unit_name_et);

    }


    private void observeViewModel(EditUnitViewModel viewModel) {

        viewModel.getUnit(unitId).observe(this, new Observer<Unit>() {
            @Override
            public void onChanged(@Nullable Unit value) {
                unit = value;
                nameEditText.setText(unit.name);
            }
        });

        viewModel.getResult().observe(this, new Observer<Result>() {
            @Override
            public void onChanged(@Nullable Result value) {

                if (value.getError() == null) {
                    Toast.makeText(EditUnitActivity.this, R.string.saved_changes, Toast.LENGTH_SHORT).show();
                    finish();
                } else if (value.getError() instanceof SQLiteConstraintException) {
                    Toast.makeText(EditUnitActivity.this, getResources().getString(R.string.already_exist_unit), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(EditUnitActivity.this, value.getError().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.complete_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.complete_action) {
            if (checkField()) {
                unit.name=nameEditText.getText().toString();
                viewModel.updateUnit(unit);

            }
        }
        return super.onOptionsItemSelected(item);
    }

    boolean checkField() {
        if (nameEditText.getText().toString().matches("\\W*")) {
            Toast.makeText(this, getResources().getString(R.string.correct_name_please_unit), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


}
