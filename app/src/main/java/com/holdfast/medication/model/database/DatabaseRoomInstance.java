package com.holdfast.medication.model.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.holdfast.medication.model.dataClass.Medicament;
import com.holdfast.medication.model.dataClass.MedicamentCourse;
import com.holdfast.medication.model.dataClass.Reception;
import com.holdfast.medication.model.dataClass.TimeTake;
import com.holdfast.medication.model.dataClass.Unit;

import java.util.List;

public class DatabaseRoomInstance {

    public final static String DATABASE_NAME = "database";

    private static AppDatabase database;

    private DatabaseRoomInstance(Context context) {


        //context.deleteDatabase(DATABASE_NAME);
        database = Room.databaseBuilder(context,
                AppDatabase.class, DATABASE_NAME)
                .addCallback(sRoomDatabaseCallback)
                .fallbackToDestructiveMigration()
                //.addMigrations(AppDatabase.MIGRATION_1_2)
                .build();

    }

    public static AppDatabase getInstance(Context context) {
        if (database == null) {
            new DatabaseRoomInstance(context);
        }
        return database;
    }


    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {

        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            new StartAppAsync(database).execute();
        }

        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PrePopulateDbAsync(database).execute();
        }
    };


    private static class StartAppAsync extends AsyncTask<Void, Void, Void> {

        private final AppDatabase database;

        StartAppAsync(AppDatabase db) {
            database = db;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            checkedDatabaseTable();
            return null;
        }

        private void checkedDatabaseTable() {
            List<MedicamentCourse> medicamentCourseList = database.medicamentCourseDao().getAll();
            List<TimeTake> timeTakeList = database.timeTakeDao().getAll();
            List<Reception> receptionList = database.receptionDao().getAll();
            List<Medicament> medicamentList = database.medicamentDao().getAll();
            List<Unit> unitList = database.unitDao().getAll();

        }
    }

    private static class PrePopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final AppDatabase database;

        PrePopulateDbAsync(AppDatabase db) {
            database = db;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            createTestItem();
            return null;
        }


        private void createTestItem() {
            long unitId = database.unitDao().insertWidthReturnId(new Unit("шт"));
            database.unitDao().insertWidthReturnId(new Unit("мл"));
            database.unitDao().insertWidthReturnId(new Unit("мг"));
//            long medicamentId = database.medicamentDao().insertWidthReturnId(new Medicament("название", "описание"));
//
//            MedicamentCourse medicamentCourse = new MedicamentCourse("Название", "Описание", MedicamentCourse.BEFORE_EATING, 0, unitId, medicamentId, true);
//            long medicamentCourseId = database.medicamentCourseDao().insertWidthReturnId(medicamentCourse);
//
//            Calendar currentDateWithoutTime = Calendar.getInstance();
//            currentDateWithoutTime.set(Calendar.HOUR_OF_DAY, 0);
//            currentDateWithoutTime.set(Calendar.MINUTE, 0);
//            currentDateWithoutTime.set(Calendar.SECOND, 0);
//            long lastDayInclusive = 1000 * (60 * 60 * 23 + 60 * 59 + 59);
//            database.timeTakeDao().insertWidthReturnId(new TimeTake(currentDateWithoutTime.getTimeInMillis(),
//                    currentDateWithoutTime.getTimeInMillis() + lastDayInclusive,
//                    medicamentCourseId));
//
//            List<Reception> receptionList = new ArrayList<>();
//            Calendar timeTake = Calendar.getInstance();
//            timeTake.set(Calendar.SECOND, 0);
//            receptionList.add(new Reception(timeTake.getTimeInMillis(), 1f, medicamentCourseId));
//            database.receptionDao().insert(receptionList);
        }
    }
}
