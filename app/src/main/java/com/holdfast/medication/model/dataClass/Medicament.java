package com.holdfast.medication.model.dataClass;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import java.util.Calendar;

@Entity(
        tableName = "medicament",
        indices = {@Index(value = {"name"}, unique = true)}
        )
public class Medicament  {

    @PrimaryKey(autoGenerate = true)
    public long id;
    public String name;
    public String note;
    @ColumnInfo(name = "date_create")
    public long dateCreate;

    public Medicament(String name, String note) {
        this.name = name;
        this.note = note;
        dateCreate= Calendar.getInstance().getTimeInMillis();
    }


    @Override
    public String toString() {
        return name;
    }
}
