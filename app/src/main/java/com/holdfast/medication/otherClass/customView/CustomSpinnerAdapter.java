package com.holdfast.medication.otherClass.customView;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

public class CustomSpinnerAdapter<T> extends ArrayAdapter<String> {

    private List<Object> objectList;


   public CustomSpinnerAdapter(Context theContext, int theLayoutResId, List<T> objects) {
        super(theContext, theLayoutResId, convertToStringList(objects));
        setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        objectList = (List<Object>)objects;
    }

    private static List<String> convertToStringList(List<?> objects) {
        List<String> spinnerUnitNameList = new ArrayList<String>();
        for (Object elem : objects) {
            spinnerUnitNameList.add(elem.toString());
        }
        return spinnerUnitNameList;
    }

    public Object getObject(int position) {
        return objectList.get(position);
    }

    public List<Object> getObjectList() {
        return objectList;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view = super.getDropDownView(position, convertView, parent);

        return view;
    }


}
