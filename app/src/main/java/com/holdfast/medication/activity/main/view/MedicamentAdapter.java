package com.holdfast.medication.activity.main.view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.holdfast.medication.R;
import com.holdfast.medication.activity.medicamentInformation.view.MedicamentActivity;
import com.holdfast.medication.model.dataClass.Medicament;

import java.util.ArrayList;
import java.util.List;

public class MedicamentAdapter extends RecyclerView.Adapter<MedicamentAdapter.MedicamentViewHolder>{

    private List<Medicament> data;
    private Context ctx;

    public MedicamentAdapter(Context ctx) {
        this.data = new ArrayList<>();
        this.ctx = ctx;
    }

    public MedicamentAdapter(Context ctx, List<Medicament> data) {
        this.data = data;
        this.ctx = ctx;
    }


    public List<Medicament> getData() {
        return data;
    }

    public Medicament getItem(int position) {
        return data.get(position);
    }


    public void addItem(Medicament item) {
        data.add(item);
        notifyDataSetChanged();
    }

    public void setItems(List<Medicament> items) {
        data = items;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return data.size();
    }


    @Override
    public MedicamentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_item_medicament, parent, false);
        return new MedicamentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MedicamentViewHolder holder, int position) {
        holder.bind(data.get(position), ctx);
    }


    public static class MedicamentViewHolder extends RecyclerView.ViewHolder {

        private TextView nameTextView;
        private TextView noteTextView;
        private View item;


        public MedicamentViewHolder(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.name_medicament_tv);
            noteTextView = (TextView) itemView.findViewById(R.id.note_medicament_tv);
            item = itemView;

        }

        public void bind(Medicament medicament, Context ctx) {
            nameTextView.setText(medicament.name);
            noteTextView.setText(medicament.note);

            item.setOnClickListener(v -> {
                ctx.startActivity(new Intent(ctx, MedicamentActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra(Medicament.class.getCanonicalName(),
                                medicament.id)
                );
            });
        }


    }
}
