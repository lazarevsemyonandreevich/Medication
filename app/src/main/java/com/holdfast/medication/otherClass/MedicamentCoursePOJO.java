package com.holdfast.medication.otherClass;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;

import com.holdfast.medication.model.dataClass.Medicament;
import com.holdfast.medication.model.dataClass.MedicamentCourse;
import com.holdfast.medication.model.dataClass.Reception;
import com.holdfast.medication.model.dataClass.TimeTake;
import com.holdfast.medication.model.dataClass.Unit;

import java.util.List;

public class MedicamentCoursePOJO {
    @Embedded
    public MedicamentCourse medicamentCourse;
    @Relation(parentColumn = "id", entity = TimeTake.class, entityColumn = "medicament_course_id")
    public List<TimeTake> timesTake;
    @Relation(parentColumn = "unit_id", entity = Unit.class, entityColumn = "id")
    public List<Unit> units;
    @Relation(parentColumn = "medicament_id", entity = Medicament.class, entityColumn = "id")
    public List<Medicament> medicaments;
    @Relation(parentColumn = "id", entity = Reception.class, entityColumn = "medicament_course_id")
    public List<Reception> receptions;
}
