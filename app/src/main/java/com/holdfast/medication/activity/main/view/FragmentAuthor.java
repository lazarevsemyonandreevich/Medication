package com.holdfast.medication.activity.main.view;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.holdfast.medication.R;

public class FragmentAuthor extends Fragment {
    private View rootView;
    private Context context;
    private Toolbar toolbar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_author, null);
        } else {
            ((ViewGroup) container.getParent()).removeView(rootView);
        }
        context = rootView.getContext().getApplicationContext();

        setHasOptionsMenu(true);//is necessary for onCreateOptionsMenu

        setToolbar();

        return rootView;
    }

    private void setToolbar() {
        toolbar = getActivity().findViewById(R.id.main_toolbar);
        toolbar.setTitle(getResources().getString(R.string.about_the_program));
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.round_reorder_white_36);
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.author_menu, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.send_mail) {

            Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
            emailIntent.setData(Uri.parse("mailto:" + "lazarevsemyonandreevich@gmail.com"))
                    .putExtra(Intent.EXTRA_SUBJECT,"Обращение пользователя к автору приложения.")
                    .putExtra(Intent.EXTRA_TEXT,"Здравствуйте, ");

            try {
                startActivity(emailIntent);
            } catch (ActivityNotFoundException e){
                Toast.makeText(context, "Приложение электронной почты недоступно", Toast.LENGTH_SHORT).show();
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
