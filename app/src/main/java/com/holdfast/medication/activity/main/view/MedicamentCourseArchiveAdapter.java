package com.holdfast.medication.activity.main.view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.holdfast.medication.R;
import com.holdfast.medication.activity.main.viewModel.MainViewModel;
import com.holdfast.medication.activity.medicamentCourseInformation.view.MedicamentCourseActivity;
import com.holdfast.medication.model.dataClass.MedicamentCourse;
import com.holdfast.medication.otherClass.MedicamentCoursePOJO;

import java.util.ArrayList;
import java.util.List;

public class MedicamentCourseArchiveAdapter extends RecyclerView.Adapter<MedicamentCourseArchiveAdapter.MedicamentCourseArchiveViewHolder> {

    private List<MedicamentCoursePOJO> data;
    private Context ctx;
    private MainViewModel viewModel;

    public MedicamentCourseArchiveAdapter(Context ctx, MainViewModel viewModel) {
        this.data = new ArrayList<>();
        this.ctx = ctx;
        this.viewModel = viewModel;
    }


    public List<MedicamentCoursePOJO> getData() {
        return data;
    }

    public MedicamentCoursePOJO getItem(int position) {
        return data.get(position);
    }


    public void addItem(MedicamentCoursePOJO item) {
        data.add(item);
        notifyDataSetChanged();
    }

    public void setItems(List<MedicamentCoursePOJO> items) {
        data = items;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return data.size();
    }


    @Override
    public MedicamentCourseArchiveViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_item_medicament_course_archive, parent, false);
        return new MedicamentCourseArchiveViewHolder(view, viewModel);
    }

    @Override
    public void onBindViewHolder(MedicamentCourseArchiveViewHolder holder, int position) {
        holder.bind(data.get(position), ctx);
    }


    public static class MedicamentCourseArchiveViewHolder extends RecyclerView.ViewHolder {

        private TextView nameTextView;
        private TextView noteTextView;
        private ImageButton repeatMedicamentCourseImageButton;
        private View item;
        private MainViewModel viewModel;


        public MedicamentCourseArchiveViewHolder(View itemView, MainViewModel viewModel) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.name_medicament_course_tv);
            noteTextView = (TextView) itemView.findViewById(R.id.note_medicament_course_tv);
            repeatMedicamentCourseImageButton = itemView.findViewById(R.id.repeat_medicament_course_img_btn);
            item = itemView;
            this.viewModel = viewModel;
        }

        public void bind(MedicamentCoursePOJO medicamentCoursePOJO, Context ctx) {
            nameTextView.setText(medicamentCoursePOJO.medicamentCourse.name);
            noteTextView.setText(medicamentCoursePOJO.medicamentCourse.note);

            repeatMedicamentCourseImageButton.setOnClickListener(view -> {
                viewModel.repeatMedicamentCourse(medicamentCoursePOJO);
            });

            item.setOnClickListener(v -> {
                ctx.startActivity(new Intent(ctx, MedicamentCourseActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra(MedicamentCourse.class.getCanonicalName(),
                                medicamentCoursePOJO.medicamentCourse.id)
                );
            });

        }
    }

}
