package com.holdfast.medication.activity.editMedicamentCourse.viewModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.database.sqlite.SQLiteConstraintException;

import com.holdfast.medication.MedicationApplication;
import com.holdfast.medication.model.dataClass.Medicament;
import com.holdfast.medication.model.dataClass.MedicamentCourse;
import com.holdfast.medication.model.dataClass.Reception;
import com.holdfast.medication.model.dataClass.TimeTake;
import com.holdfast.medication.model.dataClass.Unit;
import com.holdfast.medication.notification.NotificationAlarmManager;
import com.holdfast.medication.otherClass.MedicamentCoursePOJO;
import com.holdfast.medication.otherClass.Result;
import com.holdfast.medication.model.service.MedicamentCourseService;
import com.holdfast.medication.model.service.MedicamentService;
import com.holdfast.medication.model.service.UnitService;

import java.util.List;

import javax.inject.Inject;

public class EditMedicamentCourseDataViewModel extends ViewModel {

    private boolean load;

    private List<Reception> receptionList;
    private Medicament medicament;
    private TimeTake timeTake;
    private Unit unit;
    private MedicamentCourse medicamentCourse;

    private LiveData<List<Medicament>> LDmedicamentList;
    private LiveData<List<Unit>> LDunitList;

    private LiveData<MedicamentCoursePOJO> LDmedicamentCourse;

    @Inject
    MedicamentCourseService medicamentCourseService;
    @Inject
    MedicamentService medicamentService;
    @Inject
    UnitService unitService;
    @Inject
    NotificationAlarmManager notificationAlarmManager;
    @Inject
    MutableLiveData<Result> result;


    public EditMedicamentCourseDataViewModel() {
        MedicationApplication.getComponent().inject(this);
        LDmedicamentList = medicamentService.getListLiveDataMedicament();
        LDunitList = unitService.getListLiveDataUnit();

    }

    public LiveData<List<Medicament>> getLDMedicamentList() {
        return LDmedicamentList;
    }

    public LiveData<List<Unit>> getLDUnitList() {
        return LDunitList;
    }

    public LiveData<Result> getResult() {
        return result;
    }


    public void updateMedicamentCourse(MedicamentCourse medicamentCourse, TimeTake timeTake) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    medicamentCourseService.update(medicamentCourse, timeTake, receptionList);
                    if (medicamentCourse.status == true)
                        notificationAlarmManager.createRemind(medicamentCourse.id);
                    result.postValue(new Result(null, Long.toString(medicamentCourse.id)));
                } catch (Exception e) {
                    if (e instanceof SQLiteConstraintException)
                        result.postValue(new Result(e, "already exist"));
                    else result.postValue(new Result(e, null));
                }
            }
        }).start();

    }

    public LiveData<MedicamentCoursePOJO> getLDMedicamentCourse(long medicamentCourseId) {
        if (LDmedicamentCourse == null)
            LDmedicamentCourse = medicamentCourseService.getLiveDataMedicamentCoursePOJO(medicamentCourseId);
        return LDmedicamentCourse;

    }


    public List<Reception> getReceptionList() {
        return receptionList;
    }

    public List<Reception> updateReceptionList(int selectedCountTake) {

        if (selectedCountTake > receptionList.size()) {
            int counter = selectedCountTake - receptionList.size();
            for (int i = 0; i < counter; i++) {
                receptionList.add(new Reception());
            }

        } else if (selectedCountTake < receptionList.size()) {
            for (int i = receptionList.size() - 1; i > selectedCountTake - 1; i--) {
                receptionList.remove(i);
            }

        }
        return receptionList;
    }

    public Medicament getMedicament() {
        return medicament;
    }


    public TimeTake getTimeTake() {
        return timeTake;
    }


    public Unit getUnit() {
        return unit;
    }


    public MedicamentCourse getMedicamentCourse() {
        return medicamentCourse;
    }

    public void setMedicamentCoursePOJO(MedicamentCoursePOJO medicamentCoursePOJO) {
        this.medicamentCourse = medicamentCoursePOJO.medicamentCourse;
        this.medicament = medicamentCoursePOJO.medicaments.get(0);
        this.receptionList = medicamentCoursePOJO.receptions;
        this.timeTake = medicamentCoursePOJO.timesTake.get(0);
        this.unit = medicamentCoursePOJO.units.get(0);
        load = true;
    }

    public boolean isLoad() {
        return load;
    }
}
