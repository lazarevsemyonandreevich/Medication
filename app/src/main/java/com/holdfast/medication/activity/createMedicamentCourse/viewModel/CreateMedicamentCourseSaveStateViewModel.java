package com.holdfast.medication.activity.createMedicamentCourse.viewModel;

import android.arch.lifecycle.ViewModel;

import com.holdfast.medication.model.dataClass.Reception;

import java.util.List;

public class CreateMedicamentCourseSaveStateViewModel extends ViewModel {
    private boolean savedState;

    private List<Reception> receptionListBuffer;
    private int medicamentSpinnerSelectedPosition;
    private int unitSpinnerSelectedPosition;
    private int countDayReceptionSpinnerSelectedPosition;
    private int eatingTypeRadioGroupIndexSelectedItem;
    private long startDateSelectedTime;
    private long endDateSelectedTime;


    public CreateMedicamentCourseSaveStateViewModel() {
    }

    public void bundle(List<Reception> receptionListBuffer, int medicamentSpinnerSelectedPosition,
                       int unitSpinnerSelectedPosition, int countDayReceptionSpinnerSelectedPosition,
                       int eatingTypeRadioGroupIndexSelectedItem,
                       long startDateSelectedTime, long endDateSelectedTime) {

        this.receptionListBuffer = receptionListBuffer;
        this.medicamentSpinnerSelectedPosition = medicamentSpinnerSelectedPosition;
        this.unitSpinnerSelectedPosition = unitSpinnerSelectedPosition;
        this.countDayReceptionSpinnerSelectedPosition = countDayReceptionSpinnerSelectedPosition;
        this.eatingTypeRadioGroupIndexSelectedItem = eatingTypeRadioGroupIndexSelectedItem;
        this.startDateSelectedTime = startDateSelectedTime;
        this.endDateSelectedTime = endDateSelectedTime;
        savedState = true;
    }

    public boolean getSavedState() {
        return savedState;
    }

    public void setSavedState(boolean savedState) {
        this.savedState = savedState;
    }



    public long getStartDateSelectedTime() {
        return startDateSelectedTime;
    }

    public long getEndDateSelectedTime() {
        return endDateSelectedTime;
    }


    public int getEatingTypeRadioGroupIndexSelectedItem() {
        return eatingTypeRadioGroupIndexSelectedItem;
    }

    public List<Reception> getReceptionListBuffer() {
        return receptionListBuffer;
    }


    public int getMedicamentSpinnerSelectedPosition() {
        return medicamentSpinnerSelectedPosition;
    }


    public int getUnitSpinnerSelectedPosition() {
        return unitSpinnerSelectedPosition;
    }


    public int getCountDayReceptionSpinnerSelectedPosition() {
        return countDayReceptionSpinnerSelectedPosition;
    }

    public String[] getDayReceptions() {
        String[] dayReceptions = new String[24];
        for (int counter = 0; counter < dayReceptions.length; ) {
            dayReceptions[counter] = Integer.toString(++counter);
        }
        return dayReceptions;
    }

}
