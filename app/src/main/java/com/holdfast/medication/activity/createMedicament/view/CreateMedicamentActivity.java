package com.holdfast.medication.activity.createMedicament.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.holdfast.medication.R;
import com.holdfast.medication.activity.createMedicament.viewModel.CreateMedicamentViewModel;
import com.holdfast.medication.model.dataClass.Medicament;
import com.holdfast.medication.otherClass.Result;

public class CreateMedicamentActivity extends AppCompatActivity {

    Toolbar toolbar;
    EditText nameEditText;
    EditText noteEditText;

    CreateMedicamentViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_medicament);

        findView();

        viewModel = ViewModelProviders.of(this).get(CreateMedicamentViewModel.class);
        observeViewModel(viewModel);
    }

    private void findView() {
        toolbar = findViewById(R.id.edit_medicament_toolbar);
        setSupportActionBar(toolbar);
        nameEditText = findViewById(R.id.edit_medicament_course_name_et);
        noteEditText = findViewById(R.id.medicament_note_et);
    }




    private void observeViewModel(CreateMedicamentViewModel viewModel) {

        viewModel.getResult().observe(this, new Observer<Result>() {
            @Override
            public void onChanged(@Nullable Result value) {
                if (value.getResult().equals("success")) {
                    Intent intent = new Intent();
                    intent.putExtra(CreateMedicamentActivity.class.getCanonicalName(), nameEditText.getText().toString());
                    setResult(RESULT_OK, intent);
                    finish();
                }

                if (value.getResult().equals("error")) {
                    if (value.getError() instanceof SQLiteConstraintException){
                        Toast.makeText(CreateMedicamentActivity.this, getResources().getString(R.string.already_exist_med), Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.complete_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.complete_action) {
            if (checkField()) {
                viewModel.createMedicament(new Medicament(nameEditText.getText().toString(), noteEditText.getText().toString()));

            }
        }
        return super.onOptionsItemSelected(item);
    }

    boolean checkField() {
        if (nameEditText.getText().toString().matches(getResources().getString(R.string.check_empty_field_med))) {
            Toast.makeText(this, getResources().getString(R.string.correct_name_please_med), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


}

