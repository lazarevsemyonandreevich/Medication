package com.holdfast.medication.notification;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;

import com.holdfast.medication.MedicationApplication;
import com.holdfast.medication.model.dataClass.Medicament;
import com.holdfast.medication.model.dataClass.MedicamentCourse;
import com.holdfast.medication.model.dataClass.NotificationAlarm;
import com.holdfast.medication.model.dataClass.Reception;
import com.holdfast.medication.model.dataClass.TimeTake;
import com.holdfast.medication.model.dataClass.Unit;
import com.holdfast.medication.model.database.AppDatabase;
import com.holdfast.medication.model.service.MedicamentCourseService;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

public class NotificationAlarmManager {

    public static final int RECEPTION_NOTIFICATION = 0;
    public static final int EAT_NOTIFICATION = 1;

    @Inject
    AppDatabase database;

    @Inject
    Context context;

    @Inject
    MedicamentCourseService medicamentCourseService;


    private static NotificationAlarmManager notificationAlarmManager;

    public static NotificationAlarmManager getInstance() {
        if (notificationAlarmManager == null)
            notificationAlarmManager = new NotificationAlarmManager();
        return notificationAlarmManager;
    }

    private NotificationAlarmManager() {
        MedicationApplication.getComponent().inject(this);
    }


    public void createRemind(long medicamentCourseId) {
        createMedicamentCourseAlarm(medicamentCourseId);
    }

    public void deleteRemind(long medicamentCourseId) {
        killOldAlarm(medicamentCourseId);
    }

    public void activateRemind(long medicamentCourseId) {
        createMedicamentCourseAlarm(medicamentCourseId);
        medicamentCourseService.updateStatus(medicamentCourseId, true);
    }

    public void deactivateRemind(long medicamentCourseId) {
        killOldAlarm(medicamentCourseId);
        medicamentCourseService.updateStatus(medicamentCourseId, false);

    }

    public void restoreReminds() {
        createMedicamentCourseAlarms();
    }


    private void killOldAlarm(long medicamentCourseId) throws NullPointerException {
        try {
            AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            List<NotificationAlarm> notificationAlarmList = database.notificationAlarmDao().getByMedicamentCourseId(medicamentCourseId);
            for (NotificationAlarm elem : notificationAlarmList) {
                database.notificationAlarmDao().delete(elem);
                PendingIntent pendingIntent = NotificationAlarm.getDeletedPendingIntent(context, elem);
                am.cancel(pendingIntent);
            }
        } catch (NullPointerException e) {
        }
    }

    private void createMedicamentCourseAlarms() {
                List<MedicamentCourse> medicamentCourseList = database.medicamentCourseDao().getAll();
                for (int i = 0; i < medicamentCourseList.size(); i++) {
                    createMedicamentCourseAlarm(medicamentCourseList.get(i).id);
                }
    }

    private void createMedicamentCourseAlarm(long medicamentCourseId) {
        MedicamentCourse medicamentCourse = database.medicamentCourseDao().getById(medicamentCourseId);
        TimeTake timeTake = database.timeTakeDao().getByMedicamentCourseId(medicamentCourse.id);
        List<Reception> receptionList = database.receptionDao().getByMedicamentCourseId(medicamentCourse.id);

        for (int i = 0; i < receptionList.size(); i++) {
            Reception reception = receptionList.get(i);
            for (int j = 0; j < timeTake.getCountDay(); j++) {
                Calendar updateTime = Calendar.getInstance();
                updateTime.setTimeInMillis(reception.time);
                updateTime.add(Calendar.DATE, j);

                if (updateTime.getTimeInMillis() >= Calendar.getInstance().getTimeInMillis()) {
                    createReceptionAlarm(medicamentCourse, reception, updateTime.getTimeInMillis());
                }
            }
        }
    }

    private void createReceptionAlarm(MedicamentCourse medicamentCourse, Reception reception, long receptionTime) {
        Medicament medicament = database.medicamentDao().getById(medicamentCourse.medicamentId);
        Unit unit = database.unitDao().getById(medicamentCourse.unitId);

        String message = String.format("Медикамент \"%s\" нужно принять в дозировке: %s %s", medicament.name, reception.dose, unit.name);
        int requestCode = (int) (medicamentCourse.id + receptionTime);
        NotificationAlarm alarm = new NotificationAlarm(medicamentCourse.id, requestCode, message, RECEPTION_NOTIFICATION);
        database.notificationAlarmDao().insert(alarm);

        PendingIntent sender = NotificationAlarm.getCreatedPendingIntent(context, alarm.medicamentCourseId, alarm.requestCode, alarm.message, alarm.type);

        createAlarm(sender, receptionTime);

        if (medicamentCourse.eatType != MedicamentCourse.WHILE_EATING && medicamentCourse.eatTime != 0)
            createEatAlarm(medicamentCourse, receptionTime);
    }

    private void createEatAlarm(MedicamentCourse medicamentCourse, final long eatTime) {
        Calendar updateTime = Calendar.getInstance();
        updateTime.setTimeInMillis(eatTime);
        Medicament medicament = database.medicamentDao().getById(medicamentCourse.medicamentId);
        String message = new String();

        switch (medicamentCourse.eatType) {
            case (MedicamentCourse.BEFORE_EATING):
                message = String.format("После приёма медикамента \"%s\" нужно поесть.", medicament.name);
                updateTime.add(Calendar.MINUTE, medicamentCourse.eatTime);
                break;
            case (MedicamentCourse.AFTER_EATING):
                message = String.format("До приёма медикамента \"%s\" нужно поесть.", medicament.name);
                updateTime.add(Calendar.MINUTE, -medicamentCourse.eatTime);
                break;
            default:
                break;
        }

        int requestCode = (int) (2 * (medicamentCourse.id + updateTime.getTimeInMillis()));
        NotificationAlarm alarm = new NotificationAlarm(medicamentCourse.id, requestCode, message, EAT_NOTIFICATION);
        database.notificationAlarmDao().insert(alarm);

        PendingIntent sender = NotificationAlarm.getCreatedPendingIntent(context, alarm.medicamentCourseId, alarm.requestCode, alarm.message, alarm.type);
        createAlarm(sender, updateTime.getTimeInMillis());
    }

    private void createAlarm(PendingIntent sender, long receptionTime) {
        android.app.AlarmManager am = (android.app.AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            am.setExactAndAllowWhileIdle(android.app.AlarmManager.RTC_WAKEUP, receptionTime, sender);
        } else
            am.setExact(android.app.AlarmManager.RTC_WAKEUP, receptionTime, sender);
    }

}
