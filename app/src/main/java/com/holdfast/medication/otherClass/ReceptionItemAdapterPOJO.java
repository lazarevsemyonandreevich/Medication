package com.holdfast.medication.otherClass;

import com.holdfast.medication.model.dataClass.Reception;
import com.holdfast.medication.model.dataClass.Unit;

public class ReceptionItemAdapterPOJO {
    public long time;
    public float dose;
    public long medicamentCourseId;
    public String unit;

    public ReceptionItemAdapterPOJO(Reception reception, Unit unit) {
        this.time = reception.time;
        this.dose = reception.dose;
        this.medicamentCourseId = reception.medicamentCourseId;
        this.unit = unit.name;
    }

    public ReceptionItemAdapterPOJO(Reception reception) {
        this.time = reception.time;
        this.dose = reception.dose;
        this.medicamentCourseId = reception.medicamentCourseId;
    }

    public void setUnit(Unit unit) {
        this.unit = unit.name;
    }
}
