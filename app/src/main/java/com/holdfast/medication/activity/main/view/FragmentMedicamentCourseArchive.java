package com.holdfast.medication.activity.main.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.holdfast.medication.MedicationApplication;
import com.holdfast.medication.R;
import com.holdfast.medication.activity.main.viewModel.MainViewModel;
import com.holdfast.medication.model.service.MedicamentCourseService;
import com.holdfast.medication.otherClass.MedicamentCoursePOJO;

import java.util.List;

import javax.inject.Inject;

public class FragmentMedicamentCourseArchive extends Fragment {

    @Inject
    Context context;
    @Inject
    MedicamentCourseService medicamentCourseService;

    private RecyclerView recyclerView;
    private MainViewModel viewModel;
    private MedicamentCourseArchiveAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        MedicationApplication.getComponent().inject(this);

        View rootView = inflater.inflate(R.layout.fragment_medcourse, null);

        setHasOptionsMenu(true);//is necessary for onCreateOptionsMenu

        setToolbar("Архив курсов медикаментов");

        recyclerView = rootView.findViewById(R.id.medicament_course_rv);

        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        setMedicamentCourseRecyclerView();

        observeViewModel(viewModel);
        return rootView;
    }



    void setToolbar( String title) {
        Toolbar toolbar = getActivity().findViewById(R.id.main_toolbar);
        toolbar.setTitle(title);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.round_reorder_white_36);
    }

    private void setMedicamentCourseRecyclerView() {
        adapter = new MedicamentCourseArchiveAdapter(context, viewModel);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(adapter);
    }


    void observeViewModel(MainViewModel viewModel) {

        viewModel.getMedicamentCourseList().observe(this, new Observer<List<MedicamentCoursePOJO>>() {
            @Override
            public void onChanged(@Nullable List<MedicamentCoursePOJO> value) {
                adapter.setItems(viewModel.getNotActualityMedicamentCourses(value));
                recyclerView.setAdapter(adapter);
            }
        });
    }


}
