package com.holdfast.medication.activity.createMedicamentCourse.view;

import android.app.TimePickerDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.holdfast.medication.R;
import com.holdfast.medication.model.dataClass.Reception;
import com.holdfast.medication.model.dataClass.Unit;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class ReceptionAdapterRecyclerView extends RecyclerView.Adapter<ReceptionAdapterRecyclerView.ReceptionViewHolder> {

    private Context ctx;
    private List<Reception> receptionList;
    private Unit unit;


    public ReceptionAdapterRecyclerView(Context ctx) {
        this.ctx = ctx;
        this.receptionList = new ArrayList<>();
        this.unit = new Unit("");

    }

    public ReceptionAdapterRecyclerView(Context ctx, List<Reception> receptionList) {
        this.ctx = ctx;
        this.receptionList = new ArrayList<>();
        this.unit = new Unit("");
        setData(receptionList);

    }

    public void setData( List<Reception> receptions) {
        if (receptionList != null) receptionList.clear();
        if (receptions != null)receptionList.addAll(receptions);
        notifyDataSetChanged();
    }

    public List<Reception> getData() {
        return receptionList;
    }

    public void update(int selectedCountTake) {

        if (selectedCountTake > receptionList.size()) {
            int counter = selectedCountTake - receptionList.size();
            for (int i = 0; i < counter; i++) {
                receptionList.add(new Reception());
            }

        } else if (selectedCountTake < receptionList.size()) {
            for (int i = receptionList.size() - 1; i > selectedCountTake - 1; i--) {
                receptionList.remove(i);
            }

        }
        notifyDataSetChanged();
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ReceptionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_item_reception_edit, parent, false);
        return new ReceptionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReceptionViewHolder holder, int position) {
        holder.bind(receptionList.get(position), unit, position, ctx);
    }

    @Override
    public int getItemCount() {
        return receptionList.size();
    }


    public Reception getItem(int position) {
        return receptionList.get(position);
    }


    class ReceptionViewHolder extends RecyclerView.ViewHolder {

        TextView positionTextView;
        TextView timeTextView;
        EditText doseTextView;
        TextView unitTextView;
        View item;


        private ReceptionViewHolder(View itemView) {
            super(itemView);
            positionTextView = itemView.findViewById(R.id.reception_number_tv);
            timeTextView = itemView.findViewById(R.id.reception_time_tv);
            doseTextView = itemView.findViewById(R.id.dose_et);
            unitTextView = itemView.findViewById(R.id.reception_unit_tv);
            item = itemView;
        }


        private void bind(Reception reception, Unit unit, int position, Context ctx) {

            positionTextView.setText(++position + ")");


            timeTextView.setOnClickListener(v -> {
                TimePickerDialog.OnTimeSetListener t = new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker viewTime, int hourOfDay, int minute) {
                        Calendar buff = Calendar.getInstance();
                        buff.set(Calendar.SECOND,0);
                        buff.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        buff.set(Calendar.MINUTE, minute);
                        reception.time = buff.getTimeInMillis();
                        timeTextView.setText(new SimpleDateFormat("HH:mm", Locale.getDefault()).format(reception.time));
                    }
                };

                new TimePickerDialog(ctx, t,
                        Calendar.getInstance().get(Calendar.HOUR_OF_DAY),
                        Calendar.getInstance().get(Calendar.MINUTE), true)
                        .show();
            });

            timeTextView.setText(new SimpleDateFormat("HH:mm", Locale.getDefault()).format(reception.time));
            doseTextView.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                    if (s.toString().equals("")){
                        reception.dose = 0f;
                    }
                    else{
                        reception.dose = Float.parseFloat(s.toString());
                    }
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }
            });

            doseTextView.setText(Float.toString(reception.dose));
            unitTextView.setText(unit.name);
        }

    }
}
