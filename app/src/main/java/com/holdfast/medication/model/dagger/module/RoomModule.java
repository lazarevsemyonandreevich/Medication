package com.holdfast.medication.model.dagger.module;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;

import com.holdfast.medication.model.dao.MedicamentCourseDao;
import com.holdfast.medication.model.dao.MedicamentDao;
import com.holdfast.medication.model.dao.ReceptionDao;
import com.holdfast.medication.model.dao.TimeTakeDao;
import com.holdfast.medication.model.dao.UnitDao;
import com.holdfast.medication.model.database.AppDatabase;
import com.holdfast.medication.model.database.DatabaseRoomInstance;
import com.holdfast.medication.otherClass.Result;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RoomModule{

    private final AppDatabase database;

    public RoomModule(Context context) {

       this.database = DatabaseRoomInstance.getInstance(context);
    }

    @Provides
    MutableLiveData<Result> provideResult(){return new MutableLiveData<>();}


    @Singleton
    @Provides
    AppDatabase provideMyRoomDatabase(){ return database; }

    @Singleton
    @Provides
    MedicamentCourseDao providesMedicamentCourseDao() {
        return database.medicamentCourseDao();
    }

    @Singleton
    @Provides
    MedicamentDao providesMedicamentDao() {
        return database.medicamentDao();
    }

    @Singleton
    @Provides
    UnitDao providesUnitDao() {
        return database.unitDao();
    }

    @Singleton
    @Provides
    ReceptionDao providesReceptionDao() {
        return database.receptionDao();
    }

    @Singleton
    @Provides
    TimeTakeDao providesTimeTakeDao() {
        return database.timeTakeDao();
    }

}
