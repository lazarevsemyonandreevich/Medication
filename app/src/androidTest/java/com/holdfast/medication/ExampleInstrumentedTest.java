package com.holdfast.medication;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.holdfast.medication.model.dao.MedicamentDao;
import com.holdfast.medication.model.dataClass.Medicament;
import com.holdfast.medication.model.database.DatabaseRoomInstance;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    private Context context;
    private MedicamentDao dao;

    private Medicament testMedicament;
    private Medicament failTestMedicament;
    private Medicament testMedicamentTwo;


    @Before
    public void setUp() throws Exception {
        context = InstrumentationRegistry.getTargetContext();
        dao = DatabaseRoomInstance.getInstance(context).medicamentDao();

        testMedicament = new Medicament("testName", "testNote");
        testMedicament.id = dao.insert(testMedicament);
        failTestMedicament = new Medicament("failTestName", "failTestNote");
        testMedicamentTwo = new Medicament("testName2", "testNote2");
        testMedicamentTwo.id = dao.insert(testMedicamentTwo);
    }


    @Test
    public void testUpdateMedicament() throws Exception {
        assertEquals(1, dao.update(testMedicament));
        assertEquals(0, dao.update(failTestMedicament));
    }

    @Test
    public void testDeleteMedicament() throws Exception {
        assertEquals(1, dao.delete(testMedicament));
        assertEquals(1, dao.delete(testMedicamentTwo));
        assertEquals(0, dao.delete(failTestMedicament));
    }

    @Test
    public void testInsertMedicament() throws Exception {
        assertTrue(testMedicament.id > 0);
    }

    @Test
    public void testDeleteAllMedicament() throws Exception {
        dao.deleteAll();
        assertEquals(0, dao.getAll().size());
    }

    @Test
    public void testGetAllMedicament() throws Exception {
        assertEquals(2, dao.getAll().size());
    }

    @Test(expected = NullPointerException.class)
    public void testGetByNameMedicament() throws Exception {
        assertTrue(testMedicament.name.equals(dao.getByName(testMedicament.name).name));
        assertTrue(testMedicament.name.equals(dao.getByName("").name));
    }

    @Test
    public void testGetAllLiveMedicament() throws Exception {
        assertTrue(dao.getAllLive()!=null);
    }


    @After
    public void cleanDatabase() {
        dao.deleteAll();
    }

}
