package com.holdfast.medication.activity.medicamentCourseInformation.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.holdfast.medication.R;
import com.holdfast.medication.model.dataClass.Reception;
import com.holdfast.medication.model.dataClass.Unit;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ReceptionAdapter extends RecyclerView.Adapter<ReceptionAdapter.ReceptionViewHolder> {

    private List<Reception> data;
    private Context ctx;
    private Unit unit;

    public ReceptionAdapter(Context ctx) {
        this.data = new ArrayList<>();
        this.ctx = ctx;
        this.unit = new Unit("");
    }

    @NonNull
    @Override
    public ReceptionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_item_reception_info, parent, false);
        return new ReceptionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReceptionViewHolder holder, int position) {
        holder.bind(data.get(position),unit, position, ctx);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<Reception> receptions, Unit unit) {
        if (receptions!=null){
            data=receptions;
            this.unit = unit;
            notifyDataSetChanged();
        }
    }

    public Reception getItem(int position) {
        return new Reception(data.get(position));
    }

    class ReceptionViewHolder extends RecyclerView.ViewHolder {

        TextView positionTextView;
        TextView timeTextView;
        TextView doseTextView;
        TextView unitTextView;
        View item;


        public ReceptionViewHolder(View itemView) {
            super(itemView);
            positionTextView = itemView.findViewById(R.id.position_tv);
            timeTextView = itemView.findViewById(R.id.time_tv);
            doseTextView = itemView.findViewById(R.id.dose_tv);
            unitTextView = itemView.findViewById(R.id.unit_tv);
            item = itemView;
        }


        public void bind(Reception reception,Unit unit, int position, Context ctx) {

            positionTextView.setText(++position + ")");
            timeTextView.setText(new SimpleDateFormat("HH:mm", Locale.getDefault()).format(reception.time));

                doseTextView.setText(new DecimalFormat("#.###").format(reception.dose));


            unitTextView.setText(unit.name);
        }

    }
}
