package com.holdfast.medication.activity.medicamentInformation.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.holdfast.medication.R;
import com.holdfast.medication.activity.editMedicament.view.EditMedicamentActivity;
import com.holdfast.medication.activity.medicamentInformation.viewModel.MedicamentViewModel;
import com.holdfast.medication.model.dataClass.Medicament;
import com.holdfast.medication.otherClass.Result;

public class MedicamentActivity extends AppCompatActivity {

    MedicamentViewModel viewModel;

    Toolbar toolbar;
    TextView noteTextView;

    long medicamentId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicament_information);

        setView();

        Intent medicamentIntent = getIntent();
        medicamentId = medicamentIntent.getLongExtra(Medicament.class.getCanonicalName(), 0);

        viewModel = ViewModelProviders.of(this).get(MedicamentViewModel.class);

        observeViewModel();
    }

    private void setView() {
        toolbar = findViewById(R.id.medicament_toolbar);
        setSupportActionBar(toolbar);
        noteTextView = findViewById(R.id.note_tv);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.information_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.edit_action) {
            Intent intent = new Intent(this, EditMedicamentActivity.class);
            intent.putExtra(MedicamentActivity.class.getCanonicalName(), medicamentId);
            startActivity(intent);
        }

        if (item.getItemId() == R.id.delete_action) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Удаление")
                    .setMessage("Вы уверены, что хотите удалить медикамент? Все курсы, где он используется, будут также удалены.")
                    .setCancelable(false)
                    .setPositiveButton("Принять", (dialog, which) -> viewModel.delMedicament(medicamentId))
                    .setNegativeButton("Отмена", (dialog, id) -> dialog.cancel());
            AlertDialog alert = builder.create();
            alert.show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void observeViewModel() {
        viewModel.getResult().observe(this, new Observer<Result>() {
            @Override
            public void onChanged(@Nullable Result value) {
                if (value.getError() == null) {
                    finish();
                } else {
                    if (value.getError() instanceof SQLiteConstraintException) {
                    }
                }
            }
        });

        viewModel.getMedicament(medicamentId).observe(this, new Observer<Medicament>() {
            @Override
            public void onChanged(@Nullable Medicament value) {
                if (value != null) {
                    toolbar.setTitle(value.name);
                    noteTextView.setText(value.note);
                }

            }
        });

    }
}
