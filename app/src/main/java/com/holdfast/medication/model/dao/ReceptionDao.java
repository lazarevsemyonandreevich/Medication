package com.holdfast.medication.model.dao;


import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.holdfast.medication.model.dataClass.Reception;

import java.util.List;
@Dao
public interface ReceptionDao {

        @Query("SELECT * FROM reception")
        List<Reception> getAll();

        @Query("SELECT * FROM reception")
        LiveData<List<Reception>> getAllLive();

        @Query("SELECT * FROM reception WHERE medicament_course_id = :medicamentCourseId")
        LiveData<List<Reception>> getByMedicamentCourseIdLive(long medicamentCourseId);

        @Query("DELETE FROM reception")
        void deleteAll();

        @Query("DELETE FROM reception WHERE medicament_course_id = :medicamentCourseId")
        void deleteByMedicamentCourseId(long medicamentCourseId);

        @Query("SELECT * FROM reception WHERE id = :id")
        Reception getById(long id);

        @Query("SELECT * FROM reception WHERE medicament_course_id = :medicamentCourseId")
        List<Reception> getByMedicamentCourseId(long medicamentCourseId);

        @Insert
        void insert(Reception reception);

        @Insert
        void insert(List<Reception> receptionList);

        @Insert
        long insertWidthReturnId(Reception reception);

        @Update
        int update(Reception reception);

        @Update
        int update(List<Reception> receptionList);

        @Delete
        int delete(Reception reception);

        @Delete
        int delete(List<Reception> receptionList);

}
