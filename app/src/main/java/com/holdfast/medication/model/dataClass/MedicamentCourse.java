package com.holdfast.medication.model.dataClass;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import java.util.Calendar;

@Entity(
        tableName = "medicament_course",
        indices = {@Index(value = {"name"}, unique = true)},
        foreignKeys = {
                @ForeignKey(entity = Unit.class, parentColumns = "id", childColumns = "unit_id", onDelete = ForeignKey.CASCADE),
                @ForeignKey(entity = Medicament.class, parentColumns = "id", childColumns = "medicament_id", onDelete = ForeignKey.CASCADE)
        }
)
public class MedicamentCourse{

    public static final int BEFORE_EATING = 0;
    public static final int WHILE_EATING = 1;
    public static final int AFTER_EATING = 2;

    @PrimaryKey(autoGenerate = true)
    public long id;
    public String name;
    public String note;
    public int eatType;
    public int eatTime;
    @ColumnInfo(name = "date_create")
    public long dateCreate;
    public boolean status;
    @ColumnInfo(name = "unit_id", index = true)
    public long unitId;
    @ColumnInfo(name = "medicament_id", index = true)
    public long medicamentId;





    public MedicamentCourse(String name, String note, int eatType, int eatTime, long unitId, long medicamentId, boolean status) {
        this.id = Calendar.getInstance().getTimeInMillis();
        this.name = name;
        this.note = note;
        this.eatType = eatType;
        this.eatTime = eatTime;
        this.unitId = unitId;
        this.medicamentId = medicamentId;
        dateCreate= Calendar.getInstance().getTimeInMillis();
        this.status=status;
    }
}

