package com.holdfast.medication.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.holdfast.medication.R;
import com.holdfast.medication.activity.medicamentCourseInformation.view.MedicamentCourseActivity;
import com.holdfast.medication.model.dataClass.MedicamentCourse;

public class NotificationReceiver extends BroadcastReceiver {


    private static final String NOTIFICATION_CHANNEL_ID = "channelId";

    private long medicamentCourseId;

    private Context context;

    private NotificationManager notificationManager;
    private NotificationChannel notificationChannel;

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            NotificationAlarmManager.getInstance().restoreReminds();
        } else if (intent.getAction().equals(Integer.toString((int) intent.getLongExtra("medicamentCourseId", 0)))) {
            medicamentCourseId = intent.getLongExtra("medicamentCourseId", 0);


            this.context = context;
            String message = intent.getStringExtra("message");
            int type = intent.getIntExtra("type", 0);

            notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            switch (type) {
                case (NotificationAlarmManager.RECEPTION_NOTIFICATION):
                    createReceptionNotification(message, context);
                    break;
                case (NotificationAlarmManager.EAT_NOTIFICATION):
                    createEatNotification(message, context);
                    break;
                default:
                    break;
            }
        } else {
            Toast.makeText(context, "Ресивер словил что-то непонятное", Toast.LENGTH_SHORT).show();
        }
    }

    private void createEatNotification(String message, Context context) {
        String title = "Пора поесть!";

        createNotificationChannel();

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.round_add_circle_24)
                .setContentTitle(title)
                .setContentText("Подробнее...")
                .setAutoCancel(true)
                .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .setTicker(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setSound(Uri.parse("android.resource://"
                        + context.getPackageName() + "/" + R.raw.tablets_in_jar))
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setLights(Color.BLUE, 3000, 3000);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

        stackBuilder
                .addNextIntentWithParentStack(new Intent(context, MedicamentCourseActivity.class)
                        .putExtra(MedicamentCourse.class.getCanonicalName(), medicamentCourseId));

        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent((int) medicamentCourseId, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPendingIntent);

        Notification notification = builder.build();
        notification.flags = notification.flags | Notification.FLAG_INSISTENT;

        notificationManager.notify((int) medicamentCourseId, notification);
    }


    public void createReceptionNotification(String message, Context context) {


        String title = "Пора принять лекарство!";

        createNotificationChannel();


        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.round_add_circle_24)
                .setContentTitle(title)
                .setContentText("Подробнее о приёме...")
                .setAutoCancel(true)
                .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .setTicker(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setSound(Uri.parse("android.resource://"
                        + context.getPackageName() + "/" + R.raw.tablets_in_jar))
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setLights(Color.BLUE, 3000, 3000);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder
                .addNextIntentWithParentStack(new Intent(context, MedicamentCourseActivity.class)
                        .putExtra(MedicamentCourse.class.getCanonicalName(), medicamentCourseId));

        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent((int) medicamentCourseId, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPendingIntent);

        Notification notification = builder.build();
        notification.flags = notification.flags | Notification.FLAG_INSISTENT;

        notificationManager.notify((int) medicamentCourseId, notification);
    }

    private void createNotificationChannel() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            if (notificationManager.getNotificationChannel(NOTIFICATION_CHANNEL_ID) != null) {
                return;
            }

            String channelName = "channelName";
            notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setDescription("Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.setVibrationPattern(new long[]{1000, 1000, 1000, 1000, 1000});

            AudioAttributes audioAttributes = new AudioAttributes.Builder().setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setLegacyStreamType(AudioManager.STREAM_NOTIFICATION)
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION_EVENT).build();
            notificationChannel.setSound(Uri.parse("android.resource://"
                    + context.getPackageName() + "/" + R.raw.tablets_in_jar), audioAttributes);
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }
}
