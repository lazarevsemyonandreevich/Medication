package com.holdfast.medication.activity.editMedicament.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.holdfast.medication.R;
import com.holdfast.medication.activity.editMedicament.viewModel.EditMedicamentViewModel;
import com.holdfast.medication.activity.medicamentInformation.view.MedicamentActivity;
import com.holdfast.medication.model.dataClass.Medicament;
import com.holdfast.medication.otherClass.Result;

public class EditMedicamentActivity extends AppCompatActivity {

    Toolbar toolbar;
    EditText nameEditText;
    EditText noteEditText;


    EditMedicamentViewModel viewModel;
    private long idMedicament;
    private Medicament medicament;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_medicament);

        findView();
        setView();

        Intent medicamentCourseIntent = getIntent();
        idMedicament = medicamentCourseIntent.getLongExtra(MedicamentActivity.class.getCanonicalName(), 0);

        viewModel = ViewModelProviders.of(this).get(EditMedicamentViewModel.class);
        observeViewModel(viewModel);
    }

    private void findView() {
        nameEditText = findViewById(R.id.edit_medicament_course_name_et);
        noteEditText = findViewById(R.id.medicament_note_et);
    }

    private void setView() {
        toolbar = findViewById(R.id.edit_medicament_toolbar);
        setSupportActionBar(toolbar);
    }



    private void observeViewModel(EditMedicamentViewModel viewModel) {


        viewModel.getMedicament(idMedicament).observe(this, new Observer<Medicament>() {
            @Override
            public void onChanged(@Nullable Medicament value) {
                medicament = value;
                nameEditText.setText(medicament.name);
                noteEditText.setText(medicament.note);
            }
        });

        viewModel.getResult().observe(this, new Observer<Result>() {
            @Override
            public void onChanged(@Nullable Result value) {

                if (value.getError() == null) {
                    Toast.makeText(EditMedicamentActivity.this, R.string.saved_changes, Toast.LENGTH_SHORT).show();
                    finish();
                } else

                if (value.getError() instanceof SQLiteConstraintException) {
                    Toast.makeText(EditMedicamentActivity.this, getResources().getString(R.string.already_exist_med), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(EditMedicamentActivity.this, value.getError().getMessage(), Toast.LENGTH_SHORT).show();

                }


            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.complete_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.complete_action) {
            if (checkField()) {
                medicament.name=nameEditText.getText().toString();
                medicament.note=noteEditText.getText().toString();
                viewModel.updateMedicament(medicament);

            }
        }
        return super.onOptionsItemSelected(item);
    }

    boolean checkField() {
        if (nameEditText.getText().toString().matches(getResources().getString(R.string.check_empty_field_med))) {
            Toast.makeText(this, getResources().getString(R.string.correct_name_please_med), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

}
