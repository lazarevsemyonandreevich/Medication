package com.holdfast.medication.activity.createMedicament.viewModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.database.sqlite.SQLiteConstraintException;

import com.holdfast.medication.MedicationApplication;
import com.holdfast.medication.model.dataClass.Medicament;
import com.holdfast.medication.otherClass.Result;
import com.holdfast.medication.model.service.MedicamentService;

import javax.inject.Inject;

public class CreateMedicamentViewModel extends ViewModel {
    @Inject
    MedicamentService medicamentService;
    @Inject
    MutableLiveData<Result> result;

    public CreateMedicamentViewModel() {
        MedicationApplication.getComponent().inject(this);
    }



    public void createMedicament(Medicament medicament) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    medicamentService.add(medicament);
                    result.postValue(new Result(null, "success"));
                } catch (SQLiteConstraintException e) {
                    result.postValue(new Result(e, "error"));
                }
            }
        }).start();
    }

    public LiveData<Result> getResult() {
        return result;
    }
}
