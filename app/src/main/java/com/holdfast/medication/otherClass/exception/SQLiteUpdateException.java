package com.holdfast.medication.otherClass.exception;

import android.database.sqlite.SQLiteConstraintException;

public class SQLiteUpdateException extends SQLiteConstraintException{

        public SQLiteUpdateException() {}

        public SQLiteUpdateException(String error) {
            super(error);
        }


}
