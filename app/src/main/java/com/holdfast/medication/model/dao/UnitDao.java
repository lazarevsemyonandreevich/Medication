package com.holdfast.medication.model.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.holdfast.medication.model.dataClass.Unit;

import java.util.List;
@Dao
public interface UnitDao {
    @Query("SELECT * FROM unit")
    List<Unit> getAll();

    @Query("SELECT * FROM unit")
    LiveData<List<Unit>> getAllLive();

    @Query("DELETE FROM unit")
    void deleteAll();

    @Query("SELECT * FROM unit WHERE id = :id")
    Unit getById(long id);

    @Query("SELECT * FROM unit WHERE id = :id")
    LiveData<Unit> getByIdLive(long id);

    @Query("DELETE FROM unit WHERE id = :id")
    void deleteById(long id);

    @Query("SELECT * FROM unit WHERE name = :name")
    Unit getByName(String name);

    @Insert
    long insert(Unit unit);

    @Insert
    long insertWidthReturnId(Unit unit);

    @Update
    void update(Unit unit);

    @Delete
    void delete(Unit unit);
}
