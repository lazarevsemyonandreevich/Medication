package com.holdfast.medication.model.dagger;

import com.holdfast.medication.MedicationApplication;
import com.holdfast.medication.activity.createMedicament.viewModel.CreateMedicamentViewModel;
import com.holdfast.medication.activity.createMedicamentCourse.viewModel.CreateMedicamentCourseDataViewModel;
import com.holdfast.medication.activity.createUnit.viewModel.CreateUnitViewModel;
import com.holdfast.medication.activity.editMedicament.viewModel.EditMedicamentViewModel;
import com.holdfast.medication.activity.editMedicamentCourse.viewModel.EditMedicamentCourseDataViewModel;
import com.holdfast.medication.activity.editUnit.viewModel.EditUnitViewModel;
import com.holdfast.medication.activity.main.view.FragmentMedicamentCourse;
import com.holdfast.medication.activity.main.view.FragmentMedicamentCourseArchive;
import com.holdfast.medication.activity.main.view.MainActivity;
import com.holdfast.medication.activity.main.view.MedicamentCourseAdapter;
import com.holdfast.medication.activity.main.viewModel.MainViewModel;
import com.holdfast.medication.activity.medicamentCourseInformation.viewModel.MedicamentCourseViewModel;
import com.holdfast.medication.activity.medicamentInformation.viewModel.MedicamentViewModel;
import com.holdfast.medication.model.dagger.module.ContextModule;
import com.holdfast.medication.model.dagger.module.NotificationModule;
import com.holdfast.medication.model.dagger.module.OtherClassModule;
import com.holdfast.medication.model.dagger.module.RoomModule;
import com.holdfast.medication.model.dagger.module.ServiceModule;
import com.holdfast.medication.model.database.DatabaseRoomInstance;
import com.holdfast.medication.model.service.MedicamentCourseService;
import com.holdfast.medication.model.service.MedicamentService;
import com.holdfast.medication.model.service.UnitService;
import com.holdfast.medication.notification.NotificationAlarmManager;
import com.holdfast.medication.notification.NotificationReceiver;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(dependencies = {}, modules = {ContextModule.class, RoomModule.class, ServiceModule.class, NotificationModule.class,
        OtherClassModule.class})
public interface AppComponent {
    void inject(MedicamentCourseService medicamentCourseService);

    void inject(MainViewModel mainViewModel);

    void inject(CreateMedicamentViewModel createMedicamentViewModel);

    void inject(CreateUnitViewModel createUnitViewModel);

    void inject(EditUnitViewModel editUnitViewModel);

    void inject(MedicamentCourseViewModel medicamentCourseViewModel);

    void inject(EditMedicamentCourseDataViewModel editMedicamentCourseDataViewModel);

    void inject(EditMedicamentViewModel editMedicamentViewModel);

    void inject(MedicamentViewModel medicamentViewModel);

    void inject(DatabaseRoomInstance databaseRoomInstance);

    void inject(MainActivity mainActivity);

    void inject(NotificationAlarmManager notificationAlarmManager);

    void inject(MedicationApplication medicationApplication);

    void inject(MedicamentService medicamentService);

    void inject(UnitService unitService);

    void inject(CreateMedicamentCourseDataViewModel createMedicamentCourseDataViewModel);

    void inject(FragmentMedicamentCourse fragmentMedicamentCourse);

    void inject(MedicamentCourseAdapter.MedicamentCourseViewHolder medicamentCourseViewHolder);

  //  void inject(FragmentTreatmentCourse fragmentTreatmentCourse);

  //  void inject(TreatmentCourseAdapter.TreatmentAdapterViewHolder treatmentAdapterViewHolder);

    void inject(FragmentMedicamentCourseArchive fragmentMedicamentCourseArchive);

    void inject(NotificationReceiver notificationReceiver);


}
