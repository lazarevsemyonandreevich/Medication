package com.holdfast.medication.activity.main.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.holdfast.medication.R;
import com.holdfast.medication.activity.createMedicament.view.CreateMedicamentActivity;
import com.holdfast.medication.activity.main.viewModel.MainViewModel;
import com.holdfast.medication.model.dataClass.Medicament;

import java.util.List;

public class FragmentMedicament extends Fragment {
    private View rootView;
    private Context context;
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private MedicamentAdapter adapter;
    private MainViewModel viewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_medicament, null);
        } else {
            ((ViewGroup) container.getParent()).removeView(rootView);
        }
        context = rootView.getContext().getApplicationContext();

        setHasOptionsMenu(true);//is necessary for onCreateOptionsMenu

        setToolbar();

        recyclerView = rootView.findViewById(R.id.medicament_rv);

        setMedicamentRecyclerView();

        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        observeViewModel(viewModel);

        return rootView;
    }

    private void setToolbar() {
        toolbar = getActivity().findViewById(R.id.main_toolbar);
        toolbar.setTitle(getResources().getString(R.string.medicament_list));
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.round_reorder_white_36);
    }

    private void setMedicamentRecyclerView() {

        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        adapter = new MedicamentAdapter(context);
        recyclerView.setAdapter(adapter);
    }

    private void observeViewModel(MainViewModel viewModel) {

        viewModel.getMedicamentList().observe(this, new Observer<List<Medicament>>() {
            @Override
            public void onChanged(@Nullable List<Medicament> value) {

                adapter.setItems(value);
                recyclerView.setAdapter(adapter);
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.preview_list_menu, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.add_action) {
            startActivity(new Intent(context, CreateMedicamentActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }
}
