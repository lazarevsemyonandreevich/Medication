package com.holdfast.medication.activity.main.view;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.holdfast.medication.activity.main.viewModel.MainViewModel;

public class DeleteUnitDialog extends DialogFragment {

    MainViewModel viewModel;
    long unitId;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Удаление")
                .setMessage("Вы уверены, что хотите удалить единицу измерения? Все курсы, где она используется, будут также удалены.")
                .setCancelable(false)
                .setPositiveButton("Принять", (dialog, which) -> {
                    viewModel.delUnit(unitId);
                })
                .setNegativeButton("Отмена", (dialog, id) -> dialog.cancel());
        return builder.create();
    }

    void setInformation(MainViewModel viewModel, long unitId){
        this.viewModel=viewModel;
        this.unitId=unitId;
    }
}
