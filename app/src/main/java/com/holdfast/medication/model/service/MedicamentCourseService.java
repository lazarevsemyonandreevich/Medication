package com.holdfast.medication.model.service;

import android.arch.lifecycle.LiveData;
import android.database.sqlite.SQLiteConstraintException;

import com.holdfast.medication.MedicationApplication;
import com.holdfast.medication.model.dataClass.MedicamentCourse;
import com.holdfast.medication.model.dataClass.Reception;
import com.holdfast.medication.model.dataClass.TimeTake;
import com.holdfast.medication.model.database.AppDatabase;
import com.holdfast.medication.otherClass.MedicamentCoursePOJO;

import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

public class MedicamentCourseService {

    @Inject
    AppDatabase database;

    public MedicamentCourseService() {
        MedicationApplication.getComponent().inject(this);
    }

    public void repeatMedicamentCourse(MedicamentCoursePOJO medicamentCoursePOJO) {
        TimeTake updateTimeTake = medicamentCoursePOJO.timesTake.get(0);
        int countDay = updateTimeTake.getCountDay() - 1;

        Calendar dayStart = Calendar.getInstance();
        dayStart.set(Calendar.HOUR_OF_DAY, 0);
        dayStart.set(Calendar.MINUTE, 0);
        dayStart.set(Calendar.SECOND, 0);
        dayStart.set(Calendar.MILLISECOND, 0);

        Calendar dayEnd = Calendar.getInstance();
        dayEnd.set(Calendar.HOUR_OF_DAY, 23);
        dayEnd.set(Calendar.MINUTE, 59);
        dayEnd.set(Calendar.SECOND, 59);
        dayStart.set(Calendar.MILLISECOND, 0);
        dayEnd.set(Calendar.DATE, dayEnd.get(Calendar.DATE) + countDay);

        updateTimeTake.dayStart = dayStart.getTimeInMillis();
        updateTimeTake.dayEnd = dayEnd.getTimeInMillis();

        if (!checkAbsolutelyActualityMedicamentCourse(updateTimeTake, medicamentCoursePOJO.receptions)) {
            dayStart.set(Calendar.DATE, dayStart.get(Calendar.DATE) + 1);
            dayEnd.set(Calendar.DATE, dayEnd.get(Calendar.DATE) + 1);
            updateTimeTake.dayStart = dayStart.getTimeInMillis();
            updateTimeTake.dayEnd = dayEnd.getTimeInMillis();
        }
        database.timeTakeDao().update(updateTimeTake);
    }

    public long add(String name, String note, int eatingType, int eatingTime, TimeTake timeTake, long unitId, long medicamentId, List<Reception> receptionList) {
        MedicamentCourse medicamentCourse = new MedicamentCourse(name, note, eatingType, eatingTime, unitId, medicamentId, true);
        long medicamentCourseId = medicamentCourse.id;

        database.medicamentCourseDao().insert(medicamentCourse);

        Collections.sort(receptionList, new ReceptionCompare());
        for (Reception elem : receptionList) {
            elem.medicamentCourseId = medicamentCourseId;
        }


        database.receptionDao().insert(receptionList);

        timeTake.medicamentCourseId = medicamentCourseId;
        database.timeTakeDao().insert(timeTake);


        return medicamentCourseId;
    }

    public void update(MedicamentCourse medicamentCourse, TimeTake timeTake, List<Reception> newReceptionList) throws SQLiteConstraintException {

        if (checkAlreadyExistByName(medicamentCourse)) {
            throw new SQLiteConstraintException();
        }

        List<Reception> test0 = database.receptionDao().getAll();


        //Объекты пропадают после данной операции
        database.receptionDao().deleteByMedicamentCourseId(medicamentCourse.id);
        //

        Collections.sort(newReceptionList, new ReceptionCompare());

        Date test11;
        for (Reception elem : newReceptionList) {
            elem.medicamentCourseId = medicamentCourse.id;
            test11 = new Date(elem.time);
            int i = 1;
        }

        List<Reception> test = database.receptionDao().getAll();

        for (Reception elem : newReceptionList) {
         database.receptionDao().insert(elem);
        }

        List<Reception> test2 = database.receptionDao().getAll();

        database.medicamentCourseDao().update(medicamentCourse);

        database.timeTakeDao().update(timeTake);

    }

    class ReceptionCompare implements Comparator<Reception> {

        @Override
        public int compare(Reception o1, Reception o2) {
            return o1.compareTo(o2);
        }
    }

    private boolean checkAlreadyExistByName(MedicamentCourse medicamentCourse) {
        List<MedicamentCourse> medicamentCourseList = database.medicamentCourseDao().getAll();
        for (MedicamentCourse elem : medicamentCourseList) {
            if (elem.name.equals(medicamentCourse.name) && elem.id != medicamentCourse.id)
                return true;
        }
        return false;
    }

    public void updateStatus(long medicamentCourseId, boolean updateStatus) {
        MedicamentCourse medicamentCourse = database.medicamentCourseDao().getById(medicamentCourseId);
        if (updateStatus == true) {
            medicamentCourse.status = true;
            database.medicamentCourseDao().update(medicamentCourse);
        } else {
            medicamentCourse.status = false;
            database.medicamentCourseDao().update(medicamentCourse);
        }
    }

    public void delete(long medicamentCourseId) {
        database.medicamentCourseDao().deleteById(medicamentCourseId);
    }


    public LiveData<MedicamentCoursePOJO> getLiveDataMedicamentCoursePOJO(long medicamentCourseId) {
        return database.medicamentCourseDao().getMedicamentCoursePOJO(medicamentCourseId);
    }

    public LiveData<List<MedicamentCoursePOJO>> getListLiveDataMedicamentCoursePOJO() {
        return database.medicamentCourseDao().getAllMedicamentCoursePOJO();
    }

    public boolean checkActualityMedicamentCourse(TimeTake timeTake, List<Reception> reception) {
        Calendar currentTime = Calendar.getInstance();
        Calendar dayStart = Calendar.getInstance();
        dayStart.setTimeInMillis(timeTake.dayStart);
        int countDay = timeTake.getCountDay();


        for (int i = 0; i < countDay; i++) {
            for (int j = 0; j < reception.size(); j++) {
                Calendar bufferCalendar = Calendar.getInstance();
                bufferCalendar.setTimeInMillis(dayStart.getTimeInMillis());
                Calendar bufferReceptionTime = Calendar.getInstance();
                bufferReceptionTime.setTimeInMillis(reception.get(j).time);
                bufferCalendar.set(Calendar.YEAR, dayStart.get(Calendar.YEAR));
                bufferCalendar.set(Calendar.MONTH, dayStart.get(Calendar.MONTH));
                bufferCalendar.set(Calendar.DATE, dayStart.get(Calendar.DATE) + i);
                bufferCalendar.set(Calendar.HOUR_OF_DAY, bufferReceptionTime.get(Calendar.HOUR_OF_DAY));
                bufferCalendar.set(Calendar.MINUTE, bufferReceptionTime.get(Calendar.MINUTE));
                if (bufferCalendar.after(currentTime)) return true;
            }
        }
        return false;
    }

    public boolean checkAbsolutelyActualityMedicamentCourse(TimeTake timeTake, List<Reception> reception) {
        Calendar currentTime = Calendar.getInstance();
        Calendar dayStart = Calendar.getInstance();
        dayStart.setTimeInMillis(timeTake.dayStart);
        int countDay = timeTake.getCountDay();

        Date test = dayStart.getTime();


        for (int i = 0; i < countDay; i++) {
            for (int j = 0; j < reception.size(); j++) {
                Calendar bufferCalendar = Calendar.getInstance();
                bufferCalendar.setTimeInMillis(dayStart.getTimeInMillis());
                Date test0 = bufferCalendar.getTime();
                Calendar bufferReceptionTime = Calendar.getInstance();
                bufferReceptionTime.setTimeInMillis(reception.get(j).time);
                bufferCalendar.set(Calendar.YEAR, dayStart.get(Calendar.YEAR));
                bufferCalendar.set(Calendar.MONTH, dayStart.get(Calendar.MONTH));
                bufferCalendar.set(Calendar.DATE, dayStart.get(Calendar.DATE) + i);
                bufferCalendar.set(Calendar.HOUR_OF_DAY, bufferReceptionTime.get(Calendar.HOUR_OF_DAY));
                bufferCalendar.set(Calendar.MINUTE, bufferReceptionTime.get(Calendar.MINUTE));
                Date test1 = currentTime.getTime();
                Date test2 = bufferCalendar.getTime();
                if (bufferCalendar.before(currentTime)) return false;
            }
        }
        return true;
    }

    public int getRemainingDaysReception(MedicamentCoursePOJO medicamentCoursePOJO) {
        final int PART_TIME = 1;

        long currentDay = Calendar.getInstance().getTimeInMillis();
        long dayStart = medicamentCoursePOJO.timesTake.get(0).dayStart;
        long dayEnd = medicamentCoursePOJO.timesTake.get(0).dayEnd;


        int days = currentDay > dayStart ?
                (int) TimeUnit.MILLISECONDS.toDays(dayEnd - currentDay) + PART_TIME :
                medicamentCoursePOJO.timesTake.get(0).getCountDay();

        if (days < 0) return 0;
        return days;
    }
}


