package com.holdfast.medication.otherClass.exception;

import android.database.sqlite.SQLiteConstraintException;

public class SQLiteNotFoundDeletedItemInDatabaseException extends SQLiteConstraintException{

        public SQLiteNotFoundDeletedItemInDatabaseException() {}

        public SQLiteNotFoundDeletedItemInDatabaseException(String error) {
            super(error);
        }


}
