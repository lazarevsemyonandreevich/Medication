package com.holdfast.medication;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.holdfast.medication.activity.editMedicamentCourse.view.EditMedicamentCourseActivity;
import com.holdfast.medication.model.database.DatabaseRoomInstance;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class EditMedicamentCourseUITest {

    @Rule
    public ActivityTestRule<EditMedicamentCourseActivity> activityUnitRule = new ActivityTestRule<>(
            EditMedicamentCourseActivity.class);

    @Test
    public void editMedicamentCourse()throws Exception{
        onView(withId(R.id.edit_medicament_course_name_et)).perform(typeText("Edit medicament course name"));
        onView(withId(R.id.edit_medicament_note_et)).perform(typeText("Edit medicament course note"));
        onView((withId(R.id.complete_action))).perform(click());
    }

    @Before
    public void setUp() throws Exception{

    }

    @After
    public void cleanDatabase() {
        Context context = InstrumentationRegistry.getTargetContext();
        context.deleteDatabase(DatabaseRoomInstance.DATABASE_NAME);
    }
}
