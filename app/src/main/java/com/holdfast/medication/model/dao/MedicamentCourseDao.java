package com.holdfast.medication.model.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.Update;

import com.holdfast.medication.model.dataClass.MedicamentCourse;
import com.holdfast.medication.otherClass.MedicamentCoursePOJO;

import java.util.List;

@Dao
public interface MedicamentCourseDao {
    @Query("SELECT * FROM medicament_course")
    LiveData<List<MedicamentCourse>> getAllLive();

    @Query("SELECT * FROM medicament_course")
    List<MedicamentCourse> getAll();

    @Query("DELETE FROM medicament_course")
    void deleteAll();

    @Query("SELECT * FROM medicament_course WHERE id = :id")
    MedicamentCourse getById(long id);

    @Query("DELETE FROM medicament_course WHERE id = :id")
    void deleteById(long id);

    @Transaction
    @Query("SELECT * FROM medicament_course WHERE id = :id")
    LiveData<MedicamentCoursePOJO> getMedicamentCoursePOJO(long id);

    @Transaction
    @Query("SELECT * FROM medicament_course")
    LiveData<List<MedicamentCoursePOJO>> getAllMedicamentCoursePOJO();
    @Insert
    void insert(MedicamentCourse medicamentCourse);

    @Insert
    long insertWidthReturnId(MedicamentCourse medicamentCourse);

    @Update
    int update(MedicamentCourse medicamentCourse);

    @Delete
    void delete(MedicamentCourse medicamentCourse);
}
