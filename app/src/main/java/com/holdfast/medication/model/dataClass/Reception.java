package com.holdfast.medication.model.dataClass;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Calendar;


@Entity(
        tableName = "reception",
        foreignKeys = @ForeignKey(entity = MedicamentCourse.class, parentColumns = "id", childColumns = "medicament_course_id", onDelete = ForeignKey.CASCADE)
)
public class Reception implements Comparable<Reception> {

    @PrimaryKey(autoGenerate = true)
    public long id;
    public long time;
    public float dose;
    @NonNull
    @ColumnInfo(name = "medicament_course_id", index = true)
    public long medicamentCourseId;

    @Ignore
    public Reception(Reception another) {
        this.id = another.id;
        this.time = another.time;
        this.dose = another.dose;
        this.medicamentCourseId = another.medicamentCourseId;
    }

    @Ignore
    public Reception() {
        this.time = Calendar.getInstance().getTimeInMillis();
    }

    @Ignore
    public Reception(long time, float dose) {
        this.time = time;
        this.dose = dose;
    }

    public Reception(long time, float dose, long medicamentCourseId) {
        this.time = time;
        this.dose = dose;
        this.medicamentCourseId = medicamentCourseId;
    }


    @Override
    public int compareTo(Reception reception) {
        Calendar o1 = Calendar.getInstance();
        o1.setTimeInMillis(time);
        Calendar o2 = Calendar.getInstance();
        o2.setTimeInMillis(reception.time);
        int checkTimeO1 = 60*o1.get(Calendar.HOUR_OF_DAY) + o1.get(Calendar.MINUTE);
        int checkTimeO2 = 60*o2.get(Calendar.HOUR_OF_DAY) + o2.get(Calendar.MINUTE);
        int cmp = checkTimeO1 > checkTimeO2 ? 1 : checkTimeO1 < checkTimeO2 ? -1 : 0;
        return cmp;
    }
}
